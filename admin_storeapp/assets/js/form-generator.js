function inputChangeHandler() {
    updateShowHide(fields);
}

function initInputs(inputs, fields) {
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        if (field.group == false) {
            if (Object.name != undefined) inputs[field.name] = field.value;
        }
        else {
            initInputs(inputs, field.inputs);
        }
    }
}

function parseField(field, element) {
    var wrapper = $("<div></div>");
    if (field.label != undefined) {
        label = $("<td class='col-md-3'></td>");
        label.html(field.label);
        if (field.required == true) {
            label.append("<span class='label label-rounded label-danger'> ??</span>");
        }
        wrapper = $("<td class='col-md-9'></td>");
    }

    wrapper.append(element);
    var help = $("<span class='help-block help-area help-style'></span>");
    if (field.help != undefined) {
        help.html(field.helper);
    }
    error = $("<span class='text-danger'></span>");
    if (field.error != undefined) {
        error.html(field.error);
        help.append(error);
    }
    if (help.text().trim() != "") {
        wrapper.append(help);
    }

    if (field.label != undefined) {
        //form-group
        element = $("<tr></tr>");
        element.append(label);
        element.append(wrapper);
    }
    else element = wrapper;
    return element;
}

function generateForm(fields, container) {
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        field.id = Math.random();
        var id = "mock-" + field.id;
        var element;

        if (field.group == false) {
            element = parseField(field, renderInputField(field));
            element.attr("id", id);
            container.append(element);
        }
        else {
            element = $("<div></div>");
            element.attr("id", id);
            if (field.array == true) {
                generateArrayForm(field, element);
            } else {
                generateForm(field.inputs, element);
            }

            element = parseField(field, element);
            container.append(element);
        }
        var isShow = true;
        if (Array.isArray(field.show)) {
            var keys = Object.keys(field.show);
            for (var j = 0; j < keys.length; j++) {
                if (inputs[keys[j]] != field.show[keys[j]]) isShow = false;
            }
        }
        if (isShow == false) element.addClass("hide");
    }
}

function updateShowHide(fields) {
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var isShow = true;
        if (field.show !== undefined) {
            console.log("testing", field.name);
            var keys = Object.keys(field.show);
            console.log("keys", keys);
            for (var j = 0; j < keys.length; j++) {
                console.log(keys[j], inputs[keys[j]], field.show[keys[j]]);
                if (inputs[keys[j]] != field.show[keys[j]]) isShow = false;
            }
        }
        if (isShow == false) {
            document.getElementById("mock-" + field.id).setAttribute("hidden", 'hidden');
        }
        else document.getElementById("mock-" + field.id).removeAttribute("hidden");
    }
}

function renderInputField(field) {
    var element = "";
    switch (field.type) {
        case "text": element = text_input(field);break;
        case "radio": element = radio_input(field);break;
        case "dropdown": element = dropdown_input(field);break;
        case "date": element = date_input(field);break;
        case "datetime": element = datetime_input(field);break;
        case "link": element = link_input(field);break;
    }

    return element;
}

function generateArrayForm(field, container) {

}


/*
Each input template must have such things
- change event handler caller
- associate input with form field node(i.e changes to the input must also happen to form field node.)
- must have a unique selector to be used in show/hide function

- if same input field exists, only visible element will be included in submit data.
 */


function text_input(field) {
    if (isView == false) {
        var element = $("<input>");
        element.attr("type", "text");
        element.addClass("form-control col-md-6");
        element.attr("value", field.value);
        element.attr("name", field.name);
        element.change(function(e) {
            field.value = e.target.value;
            hidden.val(field.value);
            inputs[field.name] = e.target.value;
            inputChangeHandler();
        });
        return element;
    } else {
        var wrapper = $("<div></div>");
        wrapper.text(field.value);
        var hidden = $("<input type='hidden'>");
        hidden.attr("name", field.name);
        hidden.val(field.value);
        wrapper.append(element);
        wrapper.append(hidden);
        return wrapper;
    }
}

function link_input(field) {
    if (isView == false) {
        var element = $("<input>");
        element.attr("type", "text");
        element.addClass("form-control");
        element.attr("value", field.value);
        element.attr("name", field.name);
        element.change(function(e) {
            field.value = e.target.value;
            inputs[field.name] = e.target.value;
            inputChangeHandler();
        });
        return element;
    } else {
        var wrapper = $("<div></div>");
        wrapper.text(field.value);
        var hidden = $("<input type='hidden'>");
        hidden.attr("name", field.name);
        hidden.val(field.value);
        wrapper.append(element);
        wrapper.append(hidden);
        return wrapper;
    }
}

function radio_input(field) {
    var options = field.options;
    var keys = Object.keys(options);
    var ret = $("<div class='form-group'></div>");
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var value = options[key];
        var element = $("<input type='radio'>");
        ret.append(element);

        element.attr("name", field.name);
        element.attr("value", key);
        if (key == field.value) {
            element.attr("checked", "checked");
        }
        var label = $("<label></label>");
        label.text(value);
        element.after(label);
    }
    ret.find("input").change(function(e) {
        field.value = e.target.value;
        inputs[field.name] = e.target.value;
        inputChangeHandler();
    });
    return ret;
}

function dropdown_input(field) {
    var options = field.options;
    var keys = Object.keys(options);
    if (isView == true) {

    }
    else {
        var ret = $("<select class='form-control col-md-6'></select>");
        ret.attr("name", field.name);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var value = options[key];
            var element = $("<option></option>");
            ret.append(element);
            element.attr("value", key);
            if (key == field.value) {
                console.log(value, field.value);
                element.attr("selected", "selected");
            }
            element.text(value);
        }

        ret.change(function(e) {
            field.value = e.target.value;
            inputs[field.name] = e.target.value;
            inputChangeHandler();
        });
        return ret;
    }

}
