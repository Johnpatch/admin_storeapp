<?php
/*Common*/
$lang["home"] = "HOME";
$lang["oem"] = "HOME";
$lang["branch"] = "HOME";
$lang["search"] = "検索";
$lang["search_keyword"] = "検索キーワード";
$lang["edit"] = "編集";
$lang["delete"] = "Delete";
$lang["yes"] = "はい";
$lang["no"] = "いいえ";
$lang["id"] = "ID";
$lang["create_date"] = "作成時間";
$lang["register"] = "登録";
$lang["menu"] = "メニュー";
$lang["manage"] = "管理";
$lang["save"] = "保存";
$lang["cancel"] = "キャンセル";
$lang["email"] = "Eメール";
$lang["login"] = "ログイン";
$lang["logout"] = "ログアウト";
$lang["reset_password"] = "パスワードのリセット";
$lang["page_count_information"] = "表示";
$lang["wrong_username_or_password"] = "ユーザー名またはパスワードが正しくありません。";
$lang["delete_confirm"] = "本当に削除しますか?";
$lang["required_field"] = "このフィールドは必須です。";
$lang["exist_id"] = "この ID はすでに存在します。";
$lang["confirm_password"] = "パスワードが正しくないことを確認します。";
$lang["create_time"] = "作成時間";

/*Password Confirm*/
$lang["password"] = "パスワード";
$lang["confirm_password"] = "パスワードの確認";
$lang["message_empty"] = "空のパスワード";
$lang["message_match"] = "パスワードが一致しません。";
$lang["message_length"] = "パスワードは少なくとも 6 文字にする必要があります。";

/*Sidebar*/
$lang["agency_list"] = "代理店一覧";
$lang["agency_register_manage"] = "代理店の登録/管理";
$lang["oem_list"] = "OEMリスト";
$lang["oem_register_manage"] = "OEM登録/管理";
$lang["branch_list"] = "ユーザーリスト";
$lang["branch_register_manage"] = "ブランチの登録/管理";
$lang["admin_basic_information"] = "管理者基本情報";
$lang["control_panel"] = "コントロールパネル";
$lang["profile"] = "admin基本情報";

/*Agency List*/
$lang["agency_register_company_order"] = "代理店登録簿及び会社オーダー";
$lang["agency_id"] = "代理店ログイン ID";
$lang["agency_register"] = "代理店登録";
$lang["domain"] = "ドメイン";
$lang["total_oems"] = "OEM 合計";
$lang["total_branches"] = "合計ブランチ";

/*OEM List*/
$lang["oem_list"] = "OEM一覧";
$lang["oem_register_company_order"] = "OEM登録 / 管理";
$lang["oem_id"] = "OEM ID";
$lang["company_name"] = "会社名";
$lang["manage_number"] = "管理番号";
$lang["company_number"] = "会社番号";
$lang["service_start_date"] = "サービス開始日";
$lang["company_no"] = "会社番号";
/*OEM Register manage*/
$lang["oem_register"] = "OEM登録 / 管理 ";
$lang["oem_register_manage"] = "OEM登録/管理";
/*Branch*/
$lang["branch_register"] = "ユーザー登録";

$lang["login_id"] = "ログイン ID";
$lang["name"] = "名前";
$lang["country"] = "カントリー";
$lang["address1"] = "アドレス1";
$lang["address2"] = "アドレス2";
$lang["google_play_url"] = "google_play_url";
$lang["app_store_url"] = "app_store_url";

$lang["branch_register_manage"] = "ユーザー登録";

/*Control Panel*/
$lang["logo"] = "ロゴ";
$lang["copyright"] = "著作権";

/*Profile*/
$lang["login_id"] = "ログイン ID";
$lang["update"] = "アップデート";