<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ADMIN_Controller extends MX_Controller
{

    protected $username;

    protected $allowed_img_types;
    protected $history;
    protected $table;

    protected $auth;

    /*
     * header information includes following contents
     * controller title, icon, description
     */
    protected $head;
    protected $perpage = 10;
    protected $perpage_options = [5, 10, 20, 50];
    protected $route_prefix;
    protected $page_title;
    protected $page_subtitle;
    protected $panel_title;
    protected $page_icon;

    //DB field name corresponding to branch_id: especially for store information
    protected $branch_id_field = 'branch_id';
    //Additional conditions to get current setting
    protected $additional_conditions = array();
    //scripts for publish url
    protected $publish_script_url;

    //scripts for publish url
    protected $confirm_script_url;

    //Indicates whether index page has preview
    protected $index_has_preview = true;

    //Indicates whether publish page has preview
    protected $publish_has_preview = true;

    //Indicates whether index page has search form
    protected $index_has_search = true;

    //Indicates whether index page has order functionf
    protected $has_order;

    //Indicates whether index page has add button
    protected $has_add;

    protected $has_questions = false;
    /*
     * Input Fields Description
     * group: label
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|`box|radio|textarea|static|dropdown|color|photo|video...]
     * default: default value if initial value is not set
     * label: input label description
     * options: optional, valid when type belongs to [radio, dropdown]
     * checked:
     * help: help block description
     * required: whether this field is requried or not:this is not used in validation rules, add to validation rules again.
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values. 'key' corresonds the key of $row array
     * showIfExternal: array(key=>value). shows this element when (key=value)
     *          value can take array values. 'key' corresonds the key of $externals array
     * rules: validation settings
     */
    protected $fields;
    protected $confirm_fields;
    /* external_fields
     * static values that this controller references
     * This values may take another table's record value or user-customized values
     * takes same value format as $fields
     */
    protected $external_fields;
    protected $has_index;

    ////flag depending on change of pagetitle,paneltitle,
    protected $title_index = false;
    protected $page_title1;
    protected $page_subtitle1;
    protected $panel_title1;
    protected $list_description1;


    //Preview settings
    protected $preview_url;
    protected $index_preview_url;
    protected $publish_preview_url;

    //Index settings

    /* Search Fields
     * name: search field name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     */
    protected $search_fields = [];

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;


    //Publish settings
    protected $form_description;
    protected $list_description;

    //Confirm settings

    //Success settings
    protected $success_return_text;

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation'));
        //$this->auth = $this->session->userdata('user');
        $this->auth = $this->get_session_data($this->token, 'user');
            
        $head['logged_in'] = $this->get_session_data($this->token, 'logged_in'); 
        $head['token'] = $this->token;
        $head['user'] = $this->get_session_data($this->token, 'user'); 
        $head['user']  = json_encode($head['user']);
        $head['user']  = json_decode($head['user']);
        $head['logo'] = $this->get_session_data($this->token, 'logo'); 
        $this->head = $head; 

        $this->load->database();
        $this->initialize();
        if(isset($this->auth['ID'])) {
            $query = "select LOGO from admin where ID=".$this->auth['ID'];
            $ret = $this->db->query($query);
            $ret = $ret->result_array();
            $this->head['logo'] = $ret[0]['LOGO'];
        }
    }

    //Initilize Custom Controller Settings
    protected function initialize()
    {

        $head['description'] = "";
        //$this->head = $head;
    }

    //redirects within this controller
    protected function redirect($url)
    {
        //return redirect($this->redirect_url($url));
        return redirect($this->redirect_url($url.'?token='.$this->token));
    }

    //redirect url within this controller
    protected function redirect_url($url)
    {
        return site_url($this->route_prefix . $url);
    }

    protected function login_check()
    {
        // if (!$this->session->userdata('logged_in')) {
//            redirect('admin');
        // }

        // $this->username = $this->session->userdata('logged_in');
        $this->username = $this->get_session_data($this->token, 'logged_in');
    }


    //Common controller logic implementation

    //Set values of all fields in $input
    protected function set_field_values(&$fields, &$input, $is_array = false)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->set_field_values($field["input"], $input, element("array", $field));
            }
            $name = element("name", $field);
            if ($name !== false) {
                $field["value"] = element($name, $input, element("default", $field, $is_array ? array() : NULL));
            }
        }
    }

    protected function set_validation_rules($fields)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->set_validation_rules($field["input"]);
            }
            $name = element("name", $field);
            if ($name !== false) {
                $rules = element("rules", $field, "");
                $this->form_validation->set_rules($name, element('label', $field, $name), $rules);
            }
        }
    }

    protected function set_errors($fields, &$errors)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->set_errors($field["input"], $errors);
            }
            $name = element("name", $field);
            if ($name !== false) {
                $field["error"] = form_error($name, '<div class="error">', '</div>');
                if ($field["error"] != "") $errors[$name] = $field["error"];
            }
        }
    }

    protected function get_errors(&$fields, $errors)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->get_errors($field["input"], $errors);
            }
            $name = element("name", $field);
            if ($name !== false) {
                $field["error"] = element($name, $errors);
            }
        }
    }

    protected function get_non_array_inputs_by_fields($fields, &$input, $origin)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            $array = element("array", $field);
            if ($array == true) continue;
            if ($group === true) {
                $this->get_non_array_inputs_by_fields($field["input"], $input, $origin);
                continue;
            }
            $name = element("name", $field);
            $type = element("type", $field);
            if ($name !== false) {
                if ($type != "static") $input[$name] = $origin[$name];
                if ($type == "file" || $type == "pdf" || $type == "csv") {
                    $value = $input[$name];
                    if (strstr($value, ",")) {
                        $extension = "";
                        if ($type == "file") $extension = ".png";
                        elseif ($type == "pdf") $extension = ".pdf";
                        elseif ($type == "csv") $extension = ".csv";
                        $url = "attachments/slide_images/" . random_string() . $extension;
                        file_put_contents($url, base64_decode(explode(",", $value)[1]));
                        $input[$name] = $url;
                    }
                }
            }
        }
    }

    /*
     * Typical DB Operations
     */

    //returns db values to be edited in publish page
    protected function db_get_row($id)
    {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        $row = $this->db->get($this->table)->row_array();
        return $row;
    }

    protected function db_save($id, $data)
    {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        if ($this->db->count_all_results($this->table) > 0) {//update data
            $data['update_time'] = time();
            if ($this->has_index == true) $this->db->where('id', $id);
            else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
            if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
            $this->db->set($data)->update($this->table);
        } else {
            $data['create_time'] = time();
            $data['update_time'] = time();
            $this->db->insert($this->table, $data);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    //Overridable functions for special save cases
    protected function db_before_save($id, &$data)
    {
    }

    protected function db_after_save($id, $data)
    {
    }

    protected function db_delete($id)
    {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        $this->db->delete($this->table);
    }

    protected function db_order($id_array, $order_array)
    {
        $len = count($id_array);
        $this->db->trans_start();
        $table = $this->table;
        for ($i = 0; $i < $len; $i++) {
            $id = $id_array[$i];
            $order = $order_array[$i];
            $this->db->query("UPDATE $table SET `ORDER`=$order WHERE `ID`=$id");
        }
        $this->db->trans_complete();
    }

    protected function build_query($params) {
        $this->db->where('BRANCH_ID', $this->auth["branch_id"]);
        if ($this->has_order) $this->db->order_by("ORDER", "ASC");
    }

    protected function count_rows($params)
    {
        $this->build_query($params);
        return $this->db->count_all_results($this->table);
    }

    protected function get_rows($params, $perpage, $page)
    {
        $this->build_query($params);
        return $this->db->get($this->table, $perpage, $page)->result_array();
    }

    /*
     * Search function which should be overriden in Child Controllers
     * @params: search conditions
     * @return  search result rows 
     */
    public function index($page = 0)
    {
        $user = $this->auth;
        if ($this->has_index == false) {
            $this->redirect('publish');
        }
        $data = array();

        $input = $this->input->get();
        $perpage = $this->input->get('perpage');
        if ($perpage == 0) {
            $perpage = $this->perpage;
        }
        if ($this->index_has_search) {
            $this->set_field_values($this->search_fields, $input);
        }
        $input["perpage"] = $perpage;
        $rowscount = $this->count_rows($input);
        $data['branch_id'] = $user['branch_id'];
        $data['add_url'] = $this->redirect_url('publish/');
        $data['preview_url'] = $this->index_preview_url? $this->index_preview_url : $this->preview_url;
        $data["delete_url"] = $this->redirect_url('delete/');
        $data["order_url"] = $this->redirect_url('order/');
        $data['rows'] = $this->get_rows($input, $perpage, $page);
        $data['links_pagination'] = pagination($this->redirect_url('index'), $rowscount, $perpage, count(explode("/", $this->route_prefix)) + 1);
        $data["table_fields"] = $this->table_fields;
        $data['search_fields'] = $this->search_fields;

        $data["has_order"] = $this->has_order;
        $data["has_add"] = $this->has_add;
//        $data['has_preview'] = true;
        $data['params'] = $input;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        /////////////////////////////title with index
        $data['page_title1'] = $this->page_title1;
        $data['page_subtitle1'] = $this->page_subtitle1;
        $data['panel_title1'] = $this->panel_title1;
        $data['list_description1'] = $this->list_description1;
        $data['title_index'] = $this->title_index;
        //
        $data["form_description"] = $this->form_description;
        $data["list_description"] = $this->list_description;
        $data["page_icon"] = $this->page_icon;
        $data["perpage_options"] = $this->perpage_options;
        $data["perpage"] = $perpage;
        $data["has_preview"] = $this->index_has_preview;
        $data['has_search'] = $this->index_has_search;
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/list', $data);
        $this->load->view('_parts/footer');
    }

    public function publish($id = 0)
    {
        $user = $this->auth;
        $old = $this->session->flashdata('old');
        $row = $this->db_get_row($id);
        if (is_array($old)) $row = $old;
        if ($row == NULL) $row = array();

        $this->set_field_values($this->fields, $row);
        $errors = $this->session->flashdata("errors");
        if ($errors) {
            $this->get_errors($this->fields, $errors);
        }
        $data["branch_id"] = $user['branch_id'];
        $data["id"] = $id;
        $data["fields"] = $this->fields;
        $data["confirm_url"] = $this->redirect_url('confirm') . ($id > 0 ? "/$id" : "");
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;
        $data["page_icon"] = $this->page_icon;
        $data['preview_url'] = $this->publish_preview_url ? $this->publish_preview_url : $this->preview_url;
        $data['script_url'] = $this->publish_script_url;
        $data['has_preview'] = $this->publish_has_preview;
        $data["errors"] = $this->session->flashdata("errors");
        $data["has_questions"] = $this->has_questions;
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/publish', $data);
        $this->load->view('_parts/footer');
    }

    public function confirm($id = 0)
    {
        $input = $this->input->post();

        $this->session->set_flashdata('old', $input);
        $data = $input;
        $back_url = $input["back_url"];
        $this->set_field_values($this->fields, $input);
        $this->set_validation_rules($this->fields);

        if ($this->form_validation->run() == FALSE) {
            $error = array();
            $this->set_errors($this->fields, $error);

            if (count($error)  > 0) {
                $this->session->set_flashdata("errors", $error);
                redirect($back_url);
            }
        }

        $data["fields"] = $this->fields;
//        $data["row"] = $row;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;
        $data["page_icon"] = $this->page_icon;
        $data['script_url'] = $this->confirm_script_url;
        $data["save_url"] = $this->redirect_url('save' . ($id > 0 ? "/$id" : ""));
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/confirm', $data);
        $this->load->view('_parts/footer');
    }

    public function save($id = 0)
    {
        $input = $this->input->post();

        $file_inputs = array();
        $non_array_input = array();
        $this->get_non_array_inputs_by_fields($this->fields, $non_array_input, $input);
        $non_array_input[$this->branch_id_field] = $this->auth['branch_id'];
        $non_array_input = array_merge($non_array_input, $this->additional_conditions);
        if ($this->has_index == true) $non_array_input["ID"] = $id;
        $this->db_before_save($id, $non_array_input);

        $id = $this->db_save($id, $non_array_input);
        $this->db_after_save($id, $input);
        redirect($this->route_prefix . "success" . ($id > 0 ? "/$id" : ""));
    }

    public function success($id = 0)
    {
        $data['page_title'] = $this->page_title;
        $data['back_text'] = $this->success_return_text;
        $data['back_url'] = site_url($this->has_index == true ? $this->route_prefix : $this->route_prefix . "publish");
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/success', $data);
        $this->load->view('_parts/footer');
    }

    public function delete($id) {
        $this->db_delete($id);
        $this->redirect('');
    }

    public function order() {
        $id_array = $this->input->post('ID');
        $order_array = $this->input->post('ORDER');
        if (is_array($id_array)) {
            $this->db_order($id_array, $order_array);
        }
        $this->redirect('');
    }
}
