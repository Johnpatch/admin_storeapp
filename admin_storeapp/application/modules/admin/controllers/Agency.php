<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Agency extends ADMIN_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_Model');
        $this->load->model('Oem_Model');
        $this->load->model('Branch_Model');
    }

    public function index($page = 0)
    {
        $data = $this->head;
        $data["title"] = lang("agency_list");
        $this->load->view('_parts/header', $data);
        $this->load->view('agency/list' , array('token' => $this->token));
        $this->load->view('_parts/footer');
    }

    public function read() {
        
        $displayStart = $this->input->get('iDisplayStart');
        $displayLength = $this->input->get('iDisplayLength');
        $search = $this->input->get('search');
        $sortingCols = $this->input->get('iSortingCols');

        if ($displayLength != -1) {
            $filter['start'] = $displayStart;
            $filter['limit'] = $displayLength;
            $filter['search'] = $search;
        }
        $order = array();
        for ($i = 0; $i < $sortingCols; $i++) {
            $sortCol = $this->input->get('iSortCol_' . $i);
            $sortDir = $this->input->get('sSortDir_' . $i);
            $dataProp = $this->input->get('mDataProp_' . $sortCol);

            $order[$dataProp] = $sortDir;
        }
        $filter["ADMIN_YN"] = "N";
        $list = $this->Admin_Model->all($filter, $order);

		foreach ($list as $agency) {
            $agency = $this->addTotalCount($agency);
        }
        $result['list'] = $list;
        $result['iTotalDisplayRecords'] = $this->Admin_Model->count($filter);
        $result['iTotalRecords'] = $result['iTotalDisplayRecords'];
        print_r(json_encode($result));
    }

    public function register($id = '') {
        
        $header = $this->head;

        $data['agency'] = $id ? $this->addTotalCount($this->Admin_Model->select($id)) : NULL;
        $data['token'] = $this->token;
        $this->load->view('_parts/header' , $header);
    	$this->load->view('agency/register', $data);
        $this->load->view('_parts/footer');
    }

    public function submit() {
        $data = $_POST;
        $time_field = $data["ID"] ? "UPDATE_TIME" : "CREATE_TIME";
        $data[$time_field] = strtotime(date("Y-m-d H:i:s"));
        unset($data["CONFIRM_PASSWORD"]);
        if($data["PASSWORD"] != "") {
            $data['PASSWORD'] = md5($data["PASSWORD"]);
        } else {
            unset($data['PASSWORD']);
        }
        $this->Admin_Model->save($data);
        redirect('admin/agency/list');
    }

    public function remove() {
        $id = $_POST['id'];
        $this->Admin_Model->remove($id);
        print_r(json_encode(["success" => "true"]));
    }

    public function addTotalCount($agency = NULL) {
        $id = $agency->ID;
        $oem_list = $this->Oem_Model->all(["ADMIN_ID" => $id]);
        $agency->TOTAL_OEMS = count($oem_list);
        $branch_count = 0;
        foreach ($oem_list as $oem) {
            $oem_id = $oem->ID;
            $count_temp = $this->Branch_Model->count(["PARENT_ID" => $oem_id]);
            $branch_count += $count_temp;
        }
        $agency->TOTAL_BRANCHES += $branch_count;
        return $agency;
    }
}
