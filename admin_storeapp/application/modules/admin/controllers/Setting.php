<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Setting extends ADMIN_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Setting_Model");
    }

    public function index($page = 0)
    {
        //$user = $this->session->userdata('user');
        $user = $this->get_session_data($this->token , 'user');
        /*if (!$user) {
            redirect('admin/login');
        }*/

        $data = $this->head;
        $data["title"] = lang("control_panel");
        //$setting = $this->Setting_Model->one();

        //var_dump($user);
        //exit;

        $sql = "select * from admin where ID = ".$user['ID'];
        $ret = $this->db->query($sql);
        $ret = $ret->result_array();
        $this->load->view('_parts/header', $data);
        $this->load->view('setting', [
            "setting" => $ret[0],
            "token" => $this->token
        ]);
        $this->load->view('_parts/footer');
    }

    public function submit() {
        $data = $_POST;
        if ($data["LOGO"]) {
            //$this->session->set_userdata('logo', $data["LOGO"]);
            $this->set_session_data($this->token, "logo", $data["LOGO"]);
        } else {
            unset($data["LOGO"]);
        }
        //$this->Setting_Model->save($data);
        $user = $this->get_session_data($this->token , 'user');
        $query = "update admin set LOGO='".$data['LOGO']."' , 
                                    COPYRIGHT='".$data['COPYRIGHT']."'
                                    where ID=".$user['ID'];
        $this->db->query($query);
        redirect('admin/setting'.'?token='.$this->token);
    }
}
