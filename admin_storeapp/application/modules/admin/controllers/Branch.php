<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Branch extends ADMIN_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Branch_model');
    }
    public function index($page = 0)
    {
        $data = $this->head;
        $data["title"] = "Branch List";
        $this->load->view('_parts/header', $data);
        $this->load->view('branch/list', array('token' => $this->token));
        $this->load->view('_parts/footer');
    }
    public function read()
    {
        $displayStart = $this->input->get('iDisplayStart');
        $displayLength = $this->input->get('iDisplayLength');
        $search = $this->input->get('search');
        $sortingCols = $this->input->get('iSortingCols');
        $user = $this->get_session_data($this->token , 'user');
        $filter["PARENT_ID"] = $user['ID'];
        if ($displayLength != -1) {
            $filter['start'] = $displayStart;
            $filter['limit'] = $displayLength;
            $filter['search'] = $search;
        }
        $order = array();
        for ($i = 0; $i < $sortingCols; $i++) {
            $sortCol = $this->input->get('iSortCol_' . $i);
            $sortDir = $this->input->get('sSortDir_' . $i);
            $dataProp = $this->input->get('mDataProp_' . $sortCol);
            $order[$dataProp] = $sortDir;
        }
        $list = $this->Branch_model->all($filter, $order);
        $result['list'] = $list;
        $result['iTotalDisplayRecords'] = $this->Branch_model->count($filter);
        $result['iTotalRecords'] = $result['iTotalDisplayRecords'];
        print_r(json_encode($result));
    }
    public function register($id = '') {
        $data = $this->head;
        $data['branch'] = $id ? $this->Branch_model->select($id) : NULL;
        $data["oem_id"] = $this->get_session_data($this->token , 'user')['ID'];
        $data['token'] = $this->token;
        $this->load->view('_parts/header', $this->head);
        $this->load->view('branch/register', $data);
        $this->load->view('_parts/footer');
    }
    public function submit() {
        $data = $_POST;
        $time_field = $data["ID"] ? "UPDATE_TIME" : "CREATE_TIME";
        $data[$time_field] = strtotime(date("Y-m-d H:i:s"));
        if($data["PASSWORD"] != "") {
            $data['PASSWORD'] = md5($data["PASSWORD"]);
        } else {
            unset($data["PASSWORD"]);
        }
        unset($data["CONFIRM_PASSWORD"]);
        $this->Branch_model->save($data);
        redirect('admin/branch'.'?token='.$this->token);
    }
    public function remove() {
        $id = $_POST['id'];
        $this->Branch_model->remove($id);
        print_r(json_encode(["success" => "true"]));
    }
}
