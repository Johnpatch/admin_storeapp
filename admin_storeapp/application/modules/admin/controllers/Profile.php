<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile extends ADMIN_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_Model');
        $this->load->model('Oem_Model');
    }

    public function index($page = 0)
    {
     /*   $user = $this->session->userdata('user');
        if (!$user) {
            redirect('admin/login');
        } */
        $user = $this->get_session_data($this->token , 'user');
        $user = json_encode($user);
        $user = json_decode($user);
        $data = $this->head;
        $data["title"] = lang("profile");

        $this->load->view('_parts/header', $data);
        $this->load->view('profile', array('token' => $this->token , 'user' => $user));
        $this->load->view('_parts/footer');
    }

    public function submit() {
        $data = $_POST;
        $type = $data['TYPE'];  unset($data['TYPE']);
        $data['PASSWORD'] = md5($data['PASSWORD']);
        if ($type == 'admin')
            $this->Admin_Model->save($data);
        else
            $this->Oem_Model->save($data);
        redirect('admin/profile'.'?token='.$this->token);
    }

    public function checkExist() {
        $result["exist"] = "false";
        $id = $_POST['id'];
        $already_admin = $this->Admin_Model->one(["LOGIN_ID" => $id]);
        if ($already_admin) {
            $result["exist"] ="true";
        } else {
            $already_oem = $this->Oem_Model->one(["LOGIN_ID" => $id]);
            if ($already_oem) $result["exist"] ="true";
        }
        print_r(json_encode($result));

    }
}
