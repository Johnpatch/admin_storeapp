<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Oem extends ADMIN_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Oem_model');
        $this->load->model('Branch_model');
    }

    public function index($page = 0)
    {
        $data = $this->head;
        $data["title"] = "Oem List";
        $this->load->view('_parts/header', $data);
        $this->load->view('oem/list',array('token' => $this->token));
        $this->load->view('_parts/footer');
    }

    public function read()
    {
        $displayStart = $this->input->get('iDisplayStart');
        $displayLength = $this->input->get('iDisplayLength');
        $search = $this->input->get('search');
        $sortingCols = $this->input->get('iSortingCols');

        //$user = $this->session->userdata('user');
        $user = $this->get_session_data($this->token , 'user');

        if ($displayLength != -1) {
            $filter['start'] = $displayStart;
            $filter['limit'] = $displayLength;
            $filter['search'] = $search;
        }
        $order = array();
        for ($i = 0; $i < $sortingCols; $i++) {
            $sortCol = $this->input->get('iSortCol_' . $i);
            $sortDir = $this->input->get('sSortDir_' . $i);
            $dataProp = $this->input->get('mDataProp_' . $sortCol);
            $order[$dataProp] = $sortDir;
        }
        $list = $this->Oem_model->all($filter, $order);

        foreach ($list as $oem) {
            $oem = $this->addTotalCount($oem);
        }

        $result['list'] = $list;
        $result['iTotalDisplayRecords'] = $this->Oem_model->count($filter);
        $result['iTotalRecords'] = $result['iTotalDisplayRecords'];
        print_r(json_encode($result));
    }

    public function register($id = '') {
        $data = $this->head;
        $data['oem'] = $id ? $this->addTotalCount($this->Oem_model->select($id)) : NULL;
        $data['admin_id'] = $this->get_session_data($this->token , 'user')['ID'];
        $data['token'] = $this->token;
        $this->load->view('_parts/header', $this->head);
        $this->load->view('oem/register', $data);
        $this->load->view('_parts/footer');
    }

    public function submit() {
        $data = $_POST;
        $time_field = $data["ID"] ? "UPDATE_TIME" : "CREATE_TIME";
        $data[$time_field] = strtotime(date("Y-m-d H:i:s"));
        if($data["PASSWORD"] != "") {
            $data['PASSWORD'] = md5($data["PASSWORD"]);
        } else {
            unset($data["PASSWORD"]);
        }
        unset($data["CONFIRM_PASSWORD"]);

        $this->Oem_model->save($data);
        redirect('admin/oem'.'?token='.$this->token);
    }

    public function remove() {
        $id = $_POST['id'];
        $this->Oem_model->remove($id);
        print_r(json_encode(["success" => "true"]));
    }

    public  function addTotalCount($oem) {
        $id = $oem->ID;
        $oem->TOTAL_BRANCHES = $this->Branch_model->count(["PARENT_ID" => $id]);
        return $oem;
    }
}
