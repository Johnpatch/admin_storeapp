<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home extends ADMIN_Controller
{

    public function __construct()
    {
        parent::__construct();
//        $this->load->model(array('History_model'));
    }

    public function index($page = 0)
    {
        $this->login_check();
        $data = array();
        $head = array();
        $head = $this->head;
        $head['title'] = 'Administration - Home';
        $head['description'] = '';
        $head['keywords'] = '';
        $data['page_title'] = $this->page_title;
        $data['page_subtitle'] = $this->page_subtitle;
        $data['panel_title'] = $this->panel_title;
        // $data["help"] = $this->db->where("BRANCH_ID", $this->auth['branch_id'])->get("manual")->row_array();
        $this->load->view('_parts/header', $head);
        $this->load->view('home/home', $data);
        $this->load->view('_parts/footer');
    }
    
    protected function initialize() {
        $this->page_title = lang('home');//"Basic information";
        $this->panel_title = lang('home');//"";
    
        //Success settings
        $this->success_return_text = lang('home');//"Return";
        //language setting ended

        $this->head['title'] = "Administration - Home";
        $this->head["description"] = "";
    }
    /*
     * Called from ajax
     */

    public function changePass()
    {
        $this->login_check();
        $result = $this->Home_admin_model->changePass($_POST['new_pass'], $this->username);
        if ($result == true) {
            echo 1;
        } else {
            echo 0;
        }
        $this->saveHistory('Password change for user: ' . $this->username);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('admin');
    }

}
