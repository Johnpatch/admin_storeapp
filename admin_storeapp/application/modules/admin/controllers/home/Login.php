<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends ADMIN_Controller
{
    public function generateToken($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Admin_Model");
        $this->load->model("Setting_Model");

    }

    public function token_login() {
        header("Access-Control-Allow-Origin: *");
        $info = $this->input->post();
        $setting = $this->Setting_Model->one();
        $logo = $setting && $setting->LOGO ? $setting->LOGO : base_url("assets/imgs/site-logo.png");
        $result = $this->check($info);
        if (!empty($result)) {
            /* $_SESSION['last_login'] = $result['last_login'];
            $result['branch_id'] = $result['ID'];
            $this->session->set_userdata('logged_in', $result['LOGIN_ID']);
            $this->session->set_userdata('user', $result); */
            $token = $this->generateToken();
            /*
            $this->session->set_userdata('logged_in', $result->USERID);
            $this->session->set_userdata('user', $result);
            $this->session->set_userdata('logo', $logo);
            */
            $data = array();
            $data['logged_in'] = $result['USERID'];
            $data['user'] = $result;
            $data['logo'] = $logo;
            $query = "select * from admin_token_table where BRANCH_ID=".$result['ID'];
            $ret = $this->db->query($query);
            $ret = $ret->result_array();
            if(count($ret) < 1) {
                $query = "insert into admin_token_table set TOKEN='".$token."' , BRANCH_ID = ".$result['ID'].", DATA = '".json_encode($data)."'";
            }
            else {
                $query = "update admin_token_table set TOKEN='".$token."', DATA='".json_encode($data)."' where BRANCH_ID=".$result['ID'];
            }
            $this->db->query($query);
            echo json_encode(array("status" => "success" , "token"=>$token, "admin_id" => $result['ID']));
        } else {
            echo json_encode(array("status" => "failed"));
        }
    }
/*
    public function index($page = 0)
    {
        $setting = $this->Setting_Model->one();
        $logo = $setting && $setting->LOGO ? $setting->LOGO : base_url("assets/imgs/site-logo.png");

        $data = array();
        $head = array();
        $head['title'] = 'Administration - Login';
        $head['description'] = '';
        $head['keywords'] = '';
        $this->load->view('_parts/header', $head);
        if ($this->session->userdata('logged_in')) {
            redirect('/');
        } else {
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            if ($this->form_validation->run($this)) {
                // $result = $this->Home_admin_model->loginCheck($_POST);
                $result = $this->check($_POST);
                if (!empty($result)) {
                    $this->session->set_userdata('logged_in', $result->USERID);
                    $this->session->set_userdata('user', $result);
                    $this->session->set_userdata('logo', $logo);
                    redirect('/');
                } else {
                    $this->session->set_flashdata('err_login', lang('wrong_username_or_password'));
                    redirect('admin');
                }
            }
            $this->load->view('home/login', ["logo" => $logo]);
        }
        $this->load->view('_parts/footer');
    }
*/
    public function check($values) {
        $is_admin = $this->Admin_Model->one(array(
            'LOGIN_ID' => $values['username'],
            'PASSWORD' => md5($values['password']),
        ));
        $is_admin = json_encode($is_admin);
        $is_admin = json_decode($is_admin , TRUE);
        $result = array();
        if ($is_admin['ADMIN_YN'] == 'Y') {
            $result = $is_admin;
            $result['TYPE'] = 'admin';
            $result['IS_ADMIN'] = $result['ADMIN_YN'] == 'Y';
            $result['USERID'] = $result['LOGIN_ID'];
        } else if($is_admin['ADMIN_YN'] == 'N') {
            $result = $is_admin;
            $result['TYPE'] = 'oem';
            $result['IS_ADMIN'] = false;
            $result['USERID'] = $result['LOGIN_ID'];
        } else {
            $result = null;
        }
        return $result;
    }

}
