<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Slide extends ADMIN_Controller {

    protected $table="slide_settings";
    protected $route_prefix = 'admin/design/slide/'; //Dont't forget to attach '/' to the end of the string
    protected $has_index = true; //true if this controller has list pages
    protected $publish_has_preview = false;
    protected $index_has_preview = true;

    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $publish_script_url = "design/slide";

    protected $has_add = true;
    protected $has_order = false;
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is required or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $fields;
    protected $list_description;
    protected $confirm_fields;
    protected $preview_url;


    public function __construct() {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
//        $this->load->model('Slide_model', 'model');
        $this->head['title'] = "Design Settings";
        $this->head["description"] = "";
        $this->page_title = lang('design_setting');//"Design setting";
        $this->page_subtitle = lang('slide_image_setting_editing');//"slide image setting / editing";
        $this->panel_title = lang('slide_image_settings');//"Slide image settings";

        $this->fields = [
            [
                "group"=>true,
                "label"=>lang('design_slide_publish2_posting_period'),//"Posting period",
                "required" => true,
                "between"=>"<span class=''>~</span>",
                "input"=>[
                    [
                        "name" => "VIEW_START_DATE",
                        "type" => "datetime",
                        "picker_type"=>"from",
                        "rules" => "required",
                        "default"=>date("Y-m-d H:i")
                    ],
                    [
                        "name" => "VIEW_END_DATE",
                        "type" => "datetime",
                        "picker_type"=>"to",
                        "rules" => "required",
                        "default" => "2099-12-31 23:59"
                    ]
                ]
            ],
            [
                "name" => "IMAGE",
                "type" => "file",
                "required" => true,
                "rules" => "required",
                "label" => lang('design_slide_publish2_image'),//"Image(jpeg,jpg,gif,png)",
                "help" => lang('design_slide_publish2_image_help')//"<span class='help-tab-gray'>Format</span> jpeg,jpg,gif,png<br>
                //<span class='help-tab-gray'>Recommended size</span> 640*470 Please register the image within 1MB<br>
                //<span class='help-tab-gray'>capacity</span>"
            ],
            [
                "group" => true,
                "label" => lang('design_slide_publish2_link_destination'),//"Link destination",
                "between"=>"<div class='mt-5'></div>",
                "input" => [
                    [
                        "name" => "LINK",
                        "type" => "radio",
                        "default" => "4",
                        "options" => [
                            "1" => lang('design_slide_publish2_news_events_page'),//"News/Event page",
                            "2" => lang('design_slide_publish2_coupon_page'),//"Coupon page",
                            "3" => lang('design_slide_publish2_web_page'),//"WEB page(URL specification)",
                            "4" => lang('design_slide_publish2_none')//"None"
                        ],
                    ],
                    [
                        "name" => "PAGE_ID",
                        "type" => "dropdown",
                        "default" => 0,
                        "options" => [

                        ],
                    ]
                ]

            ],
            [
                "name" => "URL",
                "type" => "text",
                "label" => lang('design_slide_publish2_url'),//"URL",
                "help" => lang('design_slide_publish2_url_help')//"<span class='help-tab-red'>When opening with as external browser</span><br>
                //[URL to link]#target=_blank</br>Please set in the above format."
            ],
            [
                "name" => "STATUS_SHOW",
                "type" => "radio",
                "label" => lang('design_slide_publish2_publishing_settings'),//"Publishing Settings",
                "default"=>"Y",
                "options" => [
                    "Y" => lang('design_slide_publish2_indicate'),//"indicate",
                    "N" => lang('design_slide_publish2_hidden')//"Hidden",
                ]
            ],
        ];

        $this->table_fields = [
            [
                "label"=>lang('fix'),//"Fix",
                "type"=>"edit",
            ],
            [
                "label"=>lang('delete'),//"Delete",
                "type"=>"delete"
            ],
            [
                "label"=>lang('store_name'),//"Store name",
                "type"=>"text",
                "name"=>"BRANCH_NAME"
            ],
            [
                "label"=>lang('display_image'),//"Display image",
                "type"=>"image",
                "name"=>"IMAGE"
            ],
            [
                "name" => "LINK",
                "type" => "radio",
                "label" => lang('link_destination'),//"Link destination",
                "options" => [
                    "1" => lang('design_slide_publish2_news_events_page'),//"News/Event page",,//"News/Event page",
                    "2" => lang('coupon_page'),//"Coupon page",
                    "3" => lang('web_page'),//"WEB page(URL specification)",
                    "4" => lang('none'),//"None"
                ]
            ],
            [
                "name"=>"STATUS_SHOW",
                "type"=>"radio",
                "label"=>lang('slide_table_status'),//"Status",
                "options" => [
                    "Y"=>lang('indicate'),//"indicate",
                    "N"=>lang('hidden'),//"hidden"
                ]
            ]
        ];

        $this->search_fields = [
            [
                "group" => true,
                "label" => lang('publication_date'),//"Publication date",
                "between"=>"<span class=''>~</span>",
                "input" => [
                    [
                        "name" => "FROM",
                        "type" => "date",
                        "class" => "slide-from",
                        "picker_type" => "from",
                    ],
                    [
                        "name" => "TO",
                        "type" => "date",
                        "class" => "slide-to",
                        "picker_type" => "to",
                    ]
                ]
            ],
            [
                "label"=>lang('status'),
                "name"=>'STATUS_SHOW',
                "type"=>'checkbox',
                "checked"=>'Y',
                "unchecked"=>'N',
                "text"=>lang('only_when_displayed'),//'Only when displayed'
            ]
        ]; //used only when this controller has index page.(i.e $this->has_index_page = true)

        $this->preview_url = site_url("preview/main/headerandfooter");
        $this->list_description= lang('register_and_set_slide');
        $this->form_description = lang('slide_register_imge');//"Register and set slide pictures/images to be displayed on the top page.";
        $this->success_return_text = lang('slide_image_return');//"Return";
    }

    protected function build_query($params) {
        $this->db
            ->select('branches.name AS BRANCH_NAME, '.$this->table.'.*')
            ->join('branches', 'branches.id = '.$this->table.'.branch_id', 'left');
        $this->db->where('branch_id', $this->auth['branch_id']);
        if (element("STATUS_SHOW", $params) == "Y") {
            $this->db->where($this->table.'.STATUS_SHOW', 'Y');
        }

        $from = element("FROM", $params);
        $to = element("TO", $params);
        if($from != NULL){
            $this->db->where("DATE(VIEW_START_DATE) >=", $from);
            $this->db->where("DATE(VIEW_END_DATE) >=", $from);
        }
        if ($to != NULL) {
            $this->db->where("DATE(VIEW_START_DATE) <=", $to);
            $this->db->where("DATE(VIEW_END_DATE) <=", $to);
        }
    }
}
