<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pagecolor extends ADMIN_Controller
{
    protected $table = 'page_color_settings';
    protected $route_prefix = 'admin/design/pagecolor/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
//    protected $page_title = "Edit design settings";
//    protected $page_subtitle = "";
//    protected $panel_title = "Page color settings";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $preview_url;

    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings
    protected $form_description="Edit settings such as the background color and text color of the application page.";
    //Success settings
    protected $success_return_text = "Return";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->page_title = lang('pagecolor_edit_design_settings');//"Edit design settings";
        $this->page_subtitle = lang('pagecolor_space');//"";
        $this->panel_title = lang('pagecolor_pagecolor_settings');//"Page color settings";
        $this->success_return_text="Return to page color setting / editing";
        $this->fields = [
//            [
//                "name"=>"COLOR_PATTERN",
//                "type"=>"color",
//                "label"=>lang('pagecolor_pattern'),//"Color pattern",
//                "required"=>true,
//                "rules"=>"required",
//                "help"=>lang('pagecolor_help_color_pattern')//"*Color settings for each item have priority."
//            ],
            [
                "name"=>"TITLE_BAR_BACK_COLOR",
                "type"=>"color",
                "label"=>lang('pagecolor_title_bar_back_color'),//"Title bar background color",
                "help"=>lang('pagecolor_help_title_bar_back_color')//"You can set the color of the title bar in the middle of the top screen of the app."
            ],
            [
                "name"=>"TITLE_BAR_FONT_COLOR",
                "type"=>"color",
                "label"=>lang('pagecolor_title_bar_font_color'),//"Title bar text color",
                "help"=>lang('pagecolor_help_title_bar_font_color')//"You can set the text color of the title bar."
            ],
            [
                "name"=>"PAGE_BACK_COLOR",
                "type"=>"color",
                "label"=>lang('pagecolor_page_back_color'),//"Page background color",
                "help"=>lang('pagecolor_help_page_back_color')//"You can set the background color of the entire app."
            ],
            /*[
                "name"=>"PAGE_BACK_IMAGE",
                "type"=>"file",
                "label"=>lang('pagecolor_page_back_image'),//"Page background image(jpeg,jpg,gif,png)",
                "help"=>lang('pagecolor_help_page_back_image')//"You can set the background color of the entire app."
            ],*/
            [
                "name"=>"PAGE_FONT_COLOR",
                "type"=>"color",
                "label"=>lang('pagecolor_page_font_color'),//"Page text color",
                "help"=>lang('pagecolor_help_page_font_color')//"You can set the text color of the application."
            ],
            [
                "name"=>"BUTTON_BACK_COLOR",
                "type"=>"color",
                "label"=>lang('pagecolor_button_back_color'),//"Button background color",
                "help"=>lang('pagecolor_help_button_back_color')//"You can set the background color of the button."
            ]
        ];

        /////////confirm
        $this->confirm_fields = [
            //            [
            //                "name"=>"COLOR_PATTERN",
            //                "type"=>"color",
            //                "label"=>lang('pagecolor_pattern'),//"Color pattern",
            //                "required"=>true,
            //                "rules"=>"required",
            //                "help"=>lang('pagecolor_help_color_pattern')//"*Color settings for each item have priority."
            //            ],
                        [
                            "name"=>"TITLE_BAR_BACK_COLOR",
                            "type"=>"color",
                            "label"=>lang('confirm_pagecolor_title_bar_back_color'),//"Title bar background color",
                            "help"=>lang('confirm_pagecolor_help_title_bar_back_color')//"You can set the color of the title bar in the middle of the top screen of the app."
                        ],
                        [
                            "name"=>"TITLE_BAR_FONT_COLOR",
                            "type"=>"color",
                            "label"=>lang('confirm_pagecolor_title_bar_font_color'),//"Title bar text color",
                            "help"=>lang('confirm_pagecolor_help_title_bar_font_color')//"You can set the text color of the title bar."
                        ],
                        [
                            "name"=>"PAGE_BACK_COLOR",
                            "type"=>"color",
                            "label"=>lang('confirm_pagecolor_page_back_color'),//"Page background color",
                            "help"=>lang('confirm_pagecolor_help_page_back_color')//"You can set the background color of the entire app."
                        ],
                        /*[
                            "name"=>"PAGE_BACK_IMAGE",
                            "type"=>"file",
                            "label"=>lang('confirm_pagecolor_page_back_image'),//"Page background image",
                            "help"=>lang('confirm_pagecolor_help_page_back_image')//"You can set the background color of the entire app."
                        ],*/
                        [
                            "name"=>"PAGE_FONT_COLOR",
                            "type"=>"color",
                            "label"=>lang('confirm_pagecolor_page_font_color'),//"Letter color",
                            "help"=>lang('confirm_pagecolor_help_page_font_color')//"You can set the text color of the application."
                        ],
                        [
                            "name"=>"BUTTON_BACK_COLOR",
                            "type"=>"color",
                            "label"=>lang('confirm_pagecolor_button_back_color'),//"Button background color",
                            "help"=>lang('confirm_pagecolor_help_button_back_color')//"You can set the background color of the button."
                        ]
                    ];
        $this->load->model('Pagecolor_model', 'model');
        $this->head['title'] = "Design Settings";
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/colorpattern");
    }
}
