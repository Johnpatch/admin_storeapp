<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Layout extends ADMIN_Controller
{
    protected $route_prefix = 'admin/design/layout/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $table = "layout_settings";
//    protected $page_title = "Design setting";
//    protected $page_subtitle = "Layout setting / edit";
//    protected $panel_title = "Layout settings";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $publish_script_url = "design/layout";
    protected $confirm_script_url = "design/confirm";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
/*    protected $fields = [
        [
            "name"=>"LAYOUT_TYPE",
            "type"=>"radio",
            "label"=>"Layout type",
            "default" => 1,
            "options" => [
                "1"=>"Panel Layout",
                "2"=>"List Layout"
            ]
        ],
        [
            "name"=>"IS_MARGIN_BETWEEN_MENUS",
            "type"=>"radio",
            "label"=>"Margin between menu",
            "default"=>"Y",
            "options" => [
                "Y"=>"With margins",
                "N"=>"No margin"
            ]
        ],
        [
            "name"=>"NEWS_BACK_COLOR",
            "type"=>"color",
            "label"=>"Latest announcement background color",
            "help"=>"You can set the background color of the latest announcement"
        ],
        [
            "name"=>"NEWS_FONT_COLOR",
            "type"=>"color",
            "label"=>"Latest notice text color",
            "help"=>"You can set the text color of the latest announcement"
        ],
        [
            "name"=>"NEWS_TITLE_COLOR",
            "type"=>"color",
            "label"=>"Latest news headline color",
            "help"=>"You can set the title color of the latest announcement"
        ],
        [
            "name"=>"COUPON_BACK_COLOR",
            "type"=>"color",
            "label"=>"Latest announcement background color",
            "help"=>"You can set the background color of the latest coupon"
        ],
        [
            "name"=>"COUPON_FONT_COLOR",
            "type"=>"color",
            "label"=>"Latest notice text color",
            "help"=>"You can set the text color of the latest coupon"
        ],
        [
            "name"=>"COUPON_TITLE_COLOR",
            "type"=>"color",
            "label"=>"Latest news headline color",
            "help"=>"You can set the title color of the latest coupon"
        ]
    ];*/

    protected $preview_url;

    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings
    protected $form_description="Edit the layout of the app top page.";
    //Success settings
    protected $success_return_text = "Return to layout setting editing";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->page_title = lang('design_setting');
        $this->page_subtitle = lang('layout_setting_edit');
        $this->panel_title = lang('layout_settings');
        $this->fields = [
            [
                "name"=>"LAYOUT_TYPE",
                "type"=>"radio",
                "inline"=>true,
                "label"=>lang('layout_type'),//"Layout type",
                "default" => 1,
                "options" => [
                    "1"=>lang('panel_layout'),//"Panel Layout",
                    "2"=>lang('list_layout')//"List Layout"
                ],
            ],
            [
                "name"=>"IS_MARGIN_BETWEEN_MENUS",
                "type"=>"radio",
                "inline"=>true,
                "label"=>lang('margin_between_menus'),//"Margin between menu",
                "default"=>"Y",
                "options" => [
                    "Y"=>lang('with_margins'),//"With margins",
                    "N"=>lang('no_margin')//"No margin"
                ]
            ],
            [
                "name"=>"NEWS_BACK_COLOR",
                "type"=>"color",
                "label"=>lang('latest_announcement'),//"Latest announcement background color",
                "help"=>lang('help_announcement'),//You can set the background color of the latest announcement"
            ],
            [
                "name"=>"NEWS_FONT_COLOR",
                "type"=>"color",
                "label"=>lang('latest_notice'),//"Latest notice text color",
                "help"=>lang('help_notice')//"You can set the text color of the latest announcement"
            ],
            [
                "name"=>"NEWS_TITLE_COLOR",
                "type"=>"color",
                "label"=>lang('latest_title'),//"Latest news headline color",
                "help"=>lang('help_title')//"You can set the title color of the latest announcement"
            ],
            /*[
                "name"=>"COUPON_BACK_COLOR",
                "type"=>"color",
                "label"=>lang('coupon_back_color'),//"Latest announcement background color",
                "help"=>lang('help_coupon_back_color'),//"You can set the background color of the latest coupon"
            ],
            [
                "name"=>"COUPON_FONT_COLOR",
                "type"=>"color",
                "label"=>lang('coupon_font_color'),//"Latest notice text color",
                "help"=>lang('help_coupon_font_color')//"You can set the text color of the latest coupon"
            ],
            [
                "name"=>"COUPON_TITLE_COLOR",
                "type"=>"color",
                "label"=>lang('coupon_title_color'),//"Latest news headline color",
                "help"=>lang('help_coupon_title_color')//"You can set the title color of the latest coupon"
            ]*/
        ];
        /////////////////////
        $this->confirm_fields = [
            [
                "name"=>"LAYOUT_TYPE",
                "type"=>"radio",
                "label"=>lang('confrim_layout_type'),//" Top layout type",
                "default" => 1,
                "options" => [
                    "1"=>lang('confirm_panel_layout'),//"Panel Layout",
                    "2"=>lang('confirm_list_layout')//"List Layout"
                ]
            ],
            [
                "name"=>"IS_MARGIN_BETWEEN_MENUS",
                "type"=>"radio",
                "label"=>lang('confrim_margin_between_menus'),//"Margin between top menus",
                "default"=>"Y",
                "options" => [
                    "Y"=>lang('confirm_with_margins'),//"With margins",
                    "N"=>lang('confirm_no_margin')//"No margin"
                ]
            ],
            [
                "name"=>"NEWS_BACK_COLOR",
                "type"=>"color",
                "label"=>lang('confirm_latest_announcement'),//"Latest announcement background color",
                "help"=>lang('confirm_help_announcement'),//You can set the background color of the latest announcement"
            ],
            [
                "name"=>"NEWS_FONT_COLOR",
                "type"=>"color",
                "label"=>lang('latest_notice'),//"Latest notice text color",
                "help"=>lang('help_notice')//"You can set the text color of the latest announcement"
            ],
            [
                "name"=>"NEWS_TITLE_COLOR",
                "type"=>"color",
                "label"=>lang('confirm_latest_title'),//"Latest news headline color",
                "help"=>lang('help_title')//"You can set the title color of the latest announcement"
            ],
            /*[
                "name"=>"COUPON_BACK_COLOR",
                "type"=>"color",
                "label"=>lang('confirm_coupon_back_color'),//"Latest coupon background color",
                "help"=>lang('help_coupon_back_color'),//"You can set the background color of the latest coupon"
            ],
            [
                "name"=>"COUPON_TITLE_COLOR",
                "type"=>"color",
                "label"=>lang('confirm_coupon_title_color'),//"Latest coupon charater color",
                "help"=>lang('confirm_help_coupon_title_color')//"You can set the title color of the latest coupon"
            ],*/
        ];
        $this->success_return_text = lang('admin_design_layout_success_return_text');//"Return to layout setting editing";
        $this->load->model('Layout_model', 'model');
        $this->head['title'] = "Design Settings";
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/layout");
    }
}
