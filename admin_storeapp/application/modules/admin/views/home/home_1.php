<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('oem_list')?></span></h4>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home') ?>" class="breadcrumb-1"><?= lang('home') ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"></a><?= lang("oem_list")?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading"><?= lang("oem_register_company_order")?></div>
        <div class="panel-body" style="padding-top:10px;">
            <div class="row">
                <div class="col-md-12">
                    <p class="content-group">
                    </p>
                    <div class="row b-margin-20">
                        <div class="col-md-6">
                            <a href='#' class='common-btn-red-small custom-btn'>
                                <?= lang('oem_register') ?>
                            </a>
                        </div>
                        <div class="col-md-6 align-right">
                            <input type="text" class="search-input" name="search_key" placeholder="<?= lang("search_keyword")?>">
                            <button class="btn common-btn-search custom-btn b-margin-5"><i class="fa fa-search"></i>&nbsp;<?= lang("search")?></button>
                        </div>
                    </div>
                    <!--table @test start-->
                    <div class="table-responsive clear-both">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th></th>
                                <th><?= lang("oem_id")?></th>
                                <th><?= lang("company_name")?></th>
                                <th><?= lang("manage_number")?></th>
                                <th><?= lang("service_start_date")?></th>
                                <th><?= lang("manage")?></th>
                                <th><?= lang("menu")?></th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>docodoor (OEM)</td>
                                    <td>dd Test</td>
                                    <td>Docodoor company</td>
                                    <td>000000000000</td>
                                    <td>2020/03/01</td>
                                    <td align="center">
                                        <a class="btn edit-btn"><?= lang("edit")?></a>
                                        <a class="btn delete-btn"><?= lang("delete")?></a>
                                    </td>
                                    <td align="center">
                                        <a class="btn edit-btn"><?= lang("edit")?></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>