<style>
    body {

        background-position: bottom  right;
        background-repeat: no-repeat;
        /*background-color:#548fd0;*/
    }

</style>
<div class="login-title"><img src="<?=$logo?>" width="100px"></div>
<div class="container">
    <div class="login-container">
        <div id="output">
            <?php
            if ($this->session->flashdata('err_login')) {
                ?>
                <div class="alert alert-danger"><?= $this->session->flashdata('err_login') ?></div>
                <?php
            }
            ?>
        </div>

        <div class="form-box">
            <form action="" method="POST" autocomplete='off'>
                <div class="align-left">
                    <label class="login-input-label"><?= lang("id")?></label>
                    <input type="text" class="b-margin-20" name="username" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                </div>
                <div class="align-left">
                    <label class="login-input-label"><?= lang('password')?></label>
                    <input type="password" name="password" value="" autocomplete="off" readonly  onfocus="this.removeAttribute('readonly');">
                </div>
                <button class="btn btn-info btn-block login login-btn" type="submit"><?= lang("login")?></button>
                <a href="#" class="reset-password"><?= lang('reset_password')?></a>
            </form>
        </div>
    </div>
</div>