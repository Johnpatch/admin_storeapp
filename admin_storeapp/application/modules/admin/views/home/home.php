<script src="<?= base_url('assets/highcharts/highcharts.js') ?>"></script>
<script src="<?= base_url('assets/highcharts/data.js') ?>"></script>
<script src="<?= base_url('assets/highcharts/drilldown.js') ?>"></script>

<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li>Home</li>
                    
                </ul>
            </div>
        </div>
    </div>
    <!-- <div class="row">
    <div class="col-md-6">
        <button class="btn common-btn-green-small custom-btn">保存する save</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-green-small custom-btn">変更する change</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-green-small custom-btn">QRをメールで送る QRCode </button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-green-small custom-btn"><i class="fa fa-check-circle"></i>&nbsp;確認 Confirm</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-green-small custom-btn"><i class="fa fa-plus-circle"></i>&nbsp;新規登録 Add</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-green-small custom-btn"><i class="fa fa-plus-circle"></i>&nbsp;新規作成 Add</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-green-big custom-btn">アプリをインストールする（Androidのみ）</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-red-small custom-btn"><i class="fa fa-check-circle"></i>&nbsp;一括操作 bulk update</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-red-small custom-btn">画像選択 upload image</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-red-small custom-btn">写真選択 upload image</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-red-medium custom-btn"><i class="fa fa-plus-circle"></i>&nbsp;上記の動画を追加する Register with this content</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-red-medium custom-btn"><i class="fa fa-plus-circle"></i>&nbsp;メニュー新規作成</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-red-medium custom-btn"><i class="fa fa-plus-circle"></i>&nbsp;クーポン新規作成</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-red-medium custom-btn"><i class="fa fa-plus-circle"></i>&nbsp;新しくお知らせを作成</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-red-medium custom-btn"><i class="fa fa-plus-circle"></i>&nbsp;スタンプ画像新規登録</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-green-medium custom-btn"><i class="fa fa-download"></i>&nbsp;CSVダウンロード</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-search custom-btn"><i class="fa fa-search"></i>&nbsp;検索 search</button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-operation-search custom-btn"><i class="fa fa-search"></i></button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-operation-mail custom-btn"><i class="fa fa-envelope-o"></i></button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-operation-trash custom-btn"><i class="fa fa-trash"></i></button>
    </div>
    <div class="col-md-6">
        <button class="btn common-btn-operation-edit custom-btn"><i class="fa fa-edit"></i></button>
    </div>
</div> -->
    <div class="panel-body home-page">
    </div>
</div>

</div>
