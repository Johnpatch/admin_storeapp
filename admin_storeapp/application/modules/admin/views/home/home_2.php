<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('oem_register_manage')?></span>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home')?>" class="breadcrumb-1"><?= lang('home')?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><?= lang('oem_register_manage')?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= lang("oem_register_company_order") ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-9">
                        <p class="content-group">
                            ※発注はユーザ管理画面でストアー申請をした日、または非公開アプリ用のURLを発行した日が自動的に発注日になります。月額課金開始日はその日の翌月1日からになります。 ※発注されたら本社からメールで請求書をお送りします。支払い日は発注日の月末までに本社の指定銀行口座にお支払いください。
                        </p>
                        <form action="" id="layout_form" method="post">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <tr>
                                            <td><?= lang("oem_id")?></td>
                                            <td><input type="text" name="oem_id" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("password")?></td>
                                            <td><input type="password" name="password" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("confirm_password")?></td>
                                            <td><input type="password" name="confirm_password" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("company_name")?></td>
                                            <td><input type="text" name="company_name" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("manage_number")?></td>
                                            <td><input type="text" name="manage_number" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("email")?></td>
                                            <td><input type="email" name="email" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("service_start_date")?></td>
                                            <td>
                                                <input class='form-control form-control-inline input-medium date-picker cursor-pointer' name='service_start_date' size='16' type='text' value=''/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <BR>
                            <div class="text-center">
                                <button type="submit" class="btn common-btn-green-small custom-btn">
                                    <?= lang('save') ?></button>
                                <a href="#" class="btn common-btn-gray-small custom-btn">
                                    <?= lang('cancel') ?></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>