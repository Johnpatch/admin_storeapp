<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('branch_register_manage')?></span>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('/').'?token='.$token?>" class="breadcrumb-1"><?= lang('home')?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><?= lang('branch_register_manage')?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-9">
                        <p class="content-group">
                            ※発注はユーザ管理画面でストアー申請をした日、または非公開アプリ用のURLを発行した日が自動的に発注日になります。月額課金開始日はその日の翌月1日からになります。 ※発注されたら本社からメールで請求書をお送りします。支払い日は発注日の月末までに本社の指定銀行口座にお支払いください。
                        </p>
                        <form action="<?=base_url('admin/branch/submit').'?token='.$token?>" id="layout_form" method="post" autocomplete="off">
                            <input autocomplete="false" name="hidden" type="text" style="display:none;">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <tr>
                                            <td><?= lang("login_id")?></td>
                                            <td class="login_id_td">
                                                <input type="text" name="LOGIN_ID" class="form-control" value="<?=$branch && isset($branch->LOGIN_ID) ? $branch->LOGIN_ID : ''?>" autocomplete="off" />
                                                <span class="required_span help-block hide" id="id_message"><?= lang("required_field")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("name")?></td>
                                            <td class="name_td">
                                                <input type="text" name="NAME" class="form-control" value="<?=$branch && isset($branch->NAME) ? $branch->NAME : ''?>" autocomplete="false" />
                                                <span class="required_span help-block hide"><?= lang("required_field")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("password")?></td>
                                            <td class="password_td">
                                                <input type="password" name="PASSWORD" class="form-control" value="" autocomplete="false">
                                                <span class="required_span help-block hide"><?= lang("required_field")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("confirm_password")?></td>
                                            <td class="confirm_password_td">
                                                <input type="password" name="CONFIRM_PASSWORD" class="form-control">
                                                <span class="required_span help-block hide"><?= lang("confirm_password")?></span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= lang("google_play_url")?></td>
                                            <td class="google_td">
                                                <input type="text" name="GOOGLE_PLAY_URL" class="form-control" value="<?=$branch && isset($branch->GOOGLE_PLAY_URL) ? $branch->GOOGLE_PLAY_URL : ''?>">
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= lang("app_store_url")?></td>
                                            <td class="app_td">
                                                <input type="text" name="APP_STORE_URL" class="form-control" value="<?=$branch && isset($branch->APP_STORE_URL) ? $branch->APP_STORE_URL : ''?>">
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= lang("email")?></td>
                                            <td class="email_td">
                                                <input type="text" name="EMAIL" class="form-control" value="<?=$branch && isset($branch->EMAIL) ? $branch->EMAIL : ''?>">
                                            </td>
                                        </tr>

                                        <div class="hidden">
                                            <input name="ADMIN_YN" value="N">
                                            <input name="ID" value="<?=$branch && isset($branch->ID) ? $branch->ID : ''?>">
                                            <input name="PARENT_ID" value="<?=$oem_id?>">
                                        </div>
                                    </tbody>
                                </table>
                            </div>
                            <BR>
                            <div class="text-center">
                                <button type="button" onclick="submitBranch();" class="btn common-btn-green-small custom-btn">
                                    <?= lang('save') ?></button>
                                <a href="javascript:history.back(-1);" class="btn common-btn-gray-small custom-btn">
                                    <?= lang('cancel') ?></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var default_id = "<?=$branch && isset($branch->LOGIN_ID) ? $branch->LOGIN_ID : ''?>";

    function submitBranch() {
        var is_validate = true;
        var id_message_empty = "<?=lang('required_field')?>";
        var id_message_exist = "<?=lang('exist_id')?>";
        $('#id_message').html('');

        if($("input[name='LOGIN_ID']").val() == "") {
            $("td.login_id_td").addClass("has-error");
            $("td.login_id_td .required_span").removeClass("hide");
            $('#id_message').html(id_message_empty);
            is_validate = false;
        } else {
            $("td.login_id_td").removeClass("has-error");
            $("td.login_id_td .required_span").addClass("hide");
        }

        if($("input[name='NAME']").val() == "") {
            $("td.name_td").addClass("has-error");
            $("td.name_td .required_span").removeClass("hide");
            is_validate = false;
        } else {
            $("td.name_td").removeClass("has-error");
            $("td.name_td .required_span").addClass("hide");
        }

        if($("input[name='PASSWORD']").val() == "" && $("input[name='ID']").val() == "") {
            $("td.password_td").addClass("has-error");
            $(".password_td .required_span").removeClass("hide");
            is_validate = false;
        } else {
            $("td.password_td").removeClass("has-error");
            $(".password_td .required_span").addClass("hide");
            if ($("input[name='PASSWORD']").val() != $("input[name='CONFIRM_PASSWORD']").val()) {
                $("td.confirm_password_td").addClass("has-error");
                $(".confirm_password_td .required_span").removeClass("hide");
                is_validate = false;
            } else {
                $("td.confirm_password_td").removeClass("has-error");
                $(".confirm_password_td .required_span").addClass("hide");
            }
        }

        if (is_validate == false) return false;


        var input_id = $('input[name="LOGIN_ID"]').val();
        if (default_id == input_id) {
            $("#layout_form").submit();
            return true;
        }
        else {

            $.ajax({
                type: "POST",
                url: "<?=base_url('admin/profile/checkExist').'?token='.$token?>",
                data: {id: input_id}
            }).done(function (data) {
                if (JSON.parse(data).exist == 'true') {
                    $("td.login_id_td").addClass("has-error");
                    $("td.login_id_td .required_span").removeClass("hide");
                    $('#id_message').html(id_message_exist);
                    return false;
                }

                $("td.login_id_td").removeClass("has-error");
                $("td.login_id_td .required_span").addClass("hide");
                $("#layout_form").submit();

            });
        }
    }
</script>