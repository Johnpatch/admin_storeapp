<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('oem_register_manage')?></span>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('/').'?token='.$token?>" class="breadcrumb-1"><?= lang('home')?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><?= lang('oem_register_manage')?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= lang("oem_register_company_order") ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-9">
                        <p class="content-group">
                            ※発注はユーザ管理画面でストアー申請をした日、または非公開アプリ用のURLを発行した日が自動的に発注日になります。月額課金開始日はその日の翌月1日からになります。 ※発注されたら本社からメールで請求書をお送りします。支払い日は発注日の月末までに本社の指定銀行口座にお支払いください。
                        </p>
                        <form action="<?=base_url('admin/oem/submit').'?token='.$token?>" id="oem_layout_form" method="post">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <tr>
                                            <td><?= lang("oem_id")?></td>
                                            <td class="oem_id_td">
                                                <input type="text" name="LOGIN_ID" class="form-control" value="<?=$oem && isset($oem->LOGIN_ID) ? $oem->LOGIN_ID : ''?>">
                                                <span class="required_span help-block hide" id="id_message"><?= lang("required_field")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("password")?></td>
                                            <td class="password_td">
                                                <input type="password" name="PASSWORD" class="form-control" value="">
                                                <span class="required_span help-block hide"><?= lang("required_field")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("confirm_password")?></td>
                                            <td class="confirm_password_td">
                                                <input type="password" name="CONFIRM_PASSWORD" class="form-control">
                                                <span class="required_span help-block hide"><?= lang("confirm_password")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("company_name")?></td>
                                            <td>
                                                <input type="text" name="COMPANY_NAME" class="form-control" value="<?=$oem && isset($oem->COMPANY_NAME) ? $oem->COMPANY_NAME : ''?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("company_no")?></td>
                                            <td><input type="text" name="COMPANY_NO" class="form-control" value="<?=$oem && isset($oem->COMPANY_NO) ? $oem->COMPANY_NO : ''?>"></td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("email")?></td>
                                            <td class="email_td">
                                                <input type="text" name="EMAIL" class="form-control" value="<?=$oem && isset($oem->EMAIL) ? $oem->EMAIL : ''?>">
                                                <span class="required_span help-block hide">Confirm password is not correct.</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("domain")?></td>
                                            <td class="domain_td">
                                                <input type="text" name="DOMAIN" class="form-control" value="<?=$oem && isset($oem->DOMAIN) ? $oem->DOMAIN : ''?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("service_start_date")?></td>
                                            <td>
                                                <input class='form-control form-control-inline input-medium date-picker' name='SERVICE_DATE' size='16' type='text' value='<?=$oem && isset($oem->SERVICE_DATE) ? $oem->SERVICE_DATE : ''?>'/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("total_branches")?></td>
                                            <td>
                                                <input class='form-control' name='TOTAL_BRANCHES' size='16' type='text' value="<?=$oem && isset($oem->TOTAL_BRANCHES) ? $oem->TOTAL_BRANCHES : '0'?>" readonly/>
                                            </td>
                                        </tr>
                                        <div class="hidden">
                                            <input name="ID" value="<?=$oem && isset($oem->ID) ? $oem->ID : ''?>">
                                            <input type="text" name="ADMIN_ID" value="<?=$admin_id?>">
                                        </div>
                                    </tbody>
                                </table>
                            </div>
                            <BR>
                            <div class="text-center">
                                <button type="button" onclick="submitOem();" class="btn common-btn-green-small custom-btn">
                                    <?= lang('save') ?></button>
                                <a href="javascript:history.back(-1);" class="btn common-btn-gray-small custom-btn">
                                    <?= lang('cancel') ?></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var default_id = "<?=$oem && isset($oem->LOGIN_ID) ? $oem->LOGIN_ID : ''?>";
    function submitOem() {
        var is_validate = true;
        var id_message_empty = "<?=lang('required_field')?>";
        var id_message_exist = "<?=lang('exist_id')?>";
        $('#id_message').html('');

        if($("input[name='LOGIN_ID']").val() == "") {
            $("td.oem_id_td").addClass("has-error");
            $("td.oem_id_td .required_span").removeClass("hide");
            $('#id_message').html(id_message_empty);
            is_validate = false;
        } else {
            $("td.oem_id_td").removeClass("has-error");
            $("td.oem_id_td .required_span").addClass("hide");
        }

        if($("input[name='EMAIL']").val() == "") {
            $("td.email_td").addClass("has-error");
            $("td.email_td .required_span").removeClass("hide");
            is_validate = false;
        } else {
            $("td.email_td").removeClass("has-error");
            $("td.email_td .required_span").addClass("hide");
        }

        if($("input[name='PASSWORD']").val() == "" && $("input[name='ID']").val() == "") {
            $("td.password_td").addClass("has-error");
            $(".password_td .required_span").removeClass("hide");
            is_validate = false;
        } else {
            $("td.password_td").removeClass("has-error");
            $(".password_td .required_span").addClass("hide");
            if ($("input[name='PASSWORD']").val() != $("input[name='CONFIRM_PASSWORD']").val()) {
                $("td.confirm_password_td").addClass("has-error");
                $(".confirm_password_td .required_span").removeClass("hide");
                is_validate = false;
            } else {
                $("td.confirm_password_td").removeClass("has-error");
                $(".confirm_password_td .required_span").addClass("hide");
            }
        }

        if (is_validate == false) return false;

        var input_id = $('input[name="LOGIN_ID"]').val();
        if (default_id == input_id) {
            $("#oem_layout_form").submit();
            return true;
        }
        else {

            $.ajax({
                type: "POST",
                url: "<?=base_url('admin/profile/checkExist').'?token='.$token?>",
                data: {id: $('input[name="LOGIN_ID"]').val()}
            }).done(function (data) {
                if (JSON.parse(data).exist == 'true') {
                    $("td.oem_id_td").addClass("has-error");
                    $("td.oem_id_td .required_span").removeClass("hide");
                    $('#id_message').html(id_message_exist);
                    return false;
                }

                $("td.oem_id_td").removeClass("has-error");
                $("td.oem_id_td .required_span").addClass("hide");
                $("#oem_layout_form").submit();

            });
        }
    }
</script>