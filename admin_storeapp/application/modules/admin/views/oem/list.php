
<link rel="stylesheet" href="<?=base_url('assets/additional/icons/icomoon/styles.css')?>">
<link rel="stylesheet" href="<?=base_url('assets/additional/icons/fontawesome/styles.min.css')?>">
<!-- <link rel="stylesheet" href="{{ asset('assets/plugins/additional/core.css') }}"> -->
<link rel="stylesheet" href="<?=base_url('assets/additional/components.css')?>">
<link rel="stylesheet" href="<?=base_url('assets/additional/colors.css') ?>">

<script type="text/javascript" src="<?=base_url('assets/additional/tables/datatables/datatables.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/tables/datatables/extensions/fixed_columns.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/tables/datatables/extensions/col_reorder.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/tables/datatables/extensions/buttons.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/forms/selects/bootstrap_select.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/forms/selects/select2.full.min.js')?>"></script>

<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('oem_list')?></span></h4>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('/').'?token='.$token ?>" class="breadcrumb-1"><?= lang('home') ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"></a><?= lang("oem_list")?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading"><?= lang("oem_register_company_order")?></div>
        <div class="panel-body" style="padding-top:10px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="row b-margin-20">
                        <div class="col-md-6">
                            <span class="input-group-btn">
                                <a href="<?=base_url("admin/oem/register").'?token='.$token?>" class="btn create-btn">
                                    <?= lang('oem_register') ?>
                                </a>
                            </span>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search_key" placeholder="<?= lang("search_keyword")?>">
                            <span class="input-group-btn">
                                <button id="btn_search" class="btn bg-search" type="button"><?= lang("search")?></button>
                            </span>
                            </div>
                        </div>
                    </div>
                    <!--table @test start-->
                    <div class="table-responsive clear-both">
                        <table id="oem_list" class="table table-striped table-bordered" style="width: 100%;">

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


    var oTable;
    var table;
    function remove(id) {
        var params = {
            'id' : id
        };
        bootbox.confirm({
            message: "<?= lang("delete_confirm")?>",
            buttons: {
                confirm: {
                    label: "<?= lang("yes")?>",
                    className: 'btn-success'
                },
                cancel: {
                    label: "<?= lang("no")?>",
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $.post("<?=base_url('admin/oem/remove').'?token='.$token?>", params, function(data, status){
                        data = JSON.parse(data);
                        if (data.success) {
                            data_reload();
                        }
                    });
                }
            }
        });
    }

    function data_reload() {
        oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load();
    }

    $(function() {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            colReorder: true,
            dom: '<<t><"pagination-nav"pl>>',   // filter
            language: {
                lengthMenu: '_MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        // Generate content for a column
        table = $('#oem_list').DataTable({
            "paging": true,    // pagination nav
            "info": false,      // Showing 1 to 17 of 17 entries
            "searching": false,
            "bSortable": true,
            "sort": true,
            "bServerSide": true,
            "bProcessing": true,
            "aoColumns": [
                {
                    "sTitle" : "", "mData": "","sWidth": 50, "class":"text-center",
                    mRender: function (data, type, row, pos) {
                        var info = table.page.info();
                        var result = Number(info.page) * Number(info.length) + Number(pos.row) + 1;
                        return result;
                    }
                },
                { "sTitle" : "<?=lang("oem_id")?>", "mData": "LOGIN_ID", "class":"text-center"},
                { "sTitle" : "<?=lang("company_name")?>", "mData": "COMPANY_NAME", "class":"text-center"},
                { "sTitle" : "<?=lang("company_number")?>", "mData": "COMPANY_NO", "class":"text-center"},
                { "sTitle" : "<?=lang("email")?>", "mData": "EMAIL", "class":"text-center"},
                { "sTitle" : "<?=lang("domain")?>", "mData": "DOMAIN", "class":"text-center"},
                { "sTitle" : "<?=lang("total_branches")?>", "mData": "TOTAL_BRANCHES", "class":"text-center"},
                { "sTitle" : "<?=lang("service_start_date")?>", "mData": "SERVICE_DATE", "class":"text-center"},
                {
                    "sTitle" : "<?=lang("manage")?>", "mData": "CREATE_TIME", "class":"text-center",
                    mRender: function (data, type, row, pos) {
                        return  '<a class="btn edit-btn" href="<?=base_url('admin/oem/register')?>/' + row.ID + '?token=<?= $token ?>' + '">' +
                            '<?= lang("edit")?>' +
                            '</a>' +
                            '<a class="btn delete-btn" onclick="remove(\'' + row.ID + '\');">' +
                            '<?= lang("delete")?>' +
                            '</a>';
                    }
                }
            ],
            "bAutoWidth": true,
            "sAjaxSource": "<?=base_url('/admin/oem/read').'?token='.$token?>",
            "fnServerParams": function (aoData) {
                aoData.push(
                    { "name" : "search",    "value" : $('input[name="search_key"]').val() },
                );
                return aoData;
            },
            "sAjaxDataProp": "list",
            scrollX: true,
            scrollCollapse: true,
            // "order": [
            //     [0, "asc"]
            // ],
            lengthMenu: [[10, 25, 50, -1], ["10", "25", "50", "All"]],
            "ideferLoading": 1,
            "bDeferRender": true,
            buttons: {
                buttons: [
                ]
            },
            initComplete: function () {
                oTable = this;
            },
            columnDefs: [{
                bSortable: false,
                targets: [0, 2, 3, 4, 5, 6, 7, 8]
            }],
        });

        setTimeout(function() {
            $(window).on('resize', function () {
                table.columns.adjust();
            });
        }, 100);

        $("#btn_search").click(function() {
            data_reload();
        });
    });

</script>