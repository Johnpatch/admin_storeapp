<script>
    function updatePreview() {
        <?php if (!$has_preview):?>
        return;
        <?php endif;?>
        var temp_form = $("#layout_form").clone();
        temp_form.attr("action", "<?= $preview_url?>");
        temp_form.attr("target", "preview-frame");
        /*temp_form.find("input").each(function(i, target) {
         $(target).attr("name", $(target).attr("name").toUpperCase());
         });*/

        $(temp_form).find('select[name="REFER_ID"]').val($('select[name="REFER_ID"]').val());
        $(temp_form).find('select[name="DELIVERY_TYPE"]').val($('select[name="DELIVERY_TYPE"]').val());

        $("body").append(temp_form);
        temp_form.submit();
        temp_form.remove();
    }
    function add_video(id, video_name, video_value, comment_name, comment_value, is_view = 'readonly') {
        readonly = is_view == 'readonly';

        var html = "<div class='video-item' id='video-item-div-" + id + "' style='margin-bottom: 20px;'>";
        html +=         "<div class='video-item'>";
        html+=              "<input type='text' onchange='updatePreview()' class='form-control "+ (readonly ? 'hidden' : '') + "' name='" + video_name + "[]' placeholder='Video URL' value='" + video_value + "'/>";
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + video_value + "</label>";
        html +=             "<input type='text' onchange='updatePreview()' class='form-control "+ (readonly ? 'hidden' : '') + "' name='" + comment_name + "[]' placeholder='comment' value='" + comment_value + "'  />"
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + "</label>";
        html +=         '<div class="display-inline ' + (readonly ? ' hidden ' : '') + '">';
        html +=             '<a class="btn photo-item-remove btn-danger" onclick= "video_remove(\'' + id + '\')">';
        html +=                 '<i class="fa fa-trash-o"></i>';
        html +=              '</button>';
        html +=         '</div>';
        html +=         '</div>';
        html +=     "</div>";

        $('.photo-comments-list').append(html);
    }

    function add_hp(id, video_name, video_value, comment_name, comment_value, is_view = 'readonly') {
        readonly = is_view == 'readonly';
        var select = $("#current_select_input").clone();

        var id=Math.random();
        select.attr("id",'select-hp-'+ id);
        select.val(video_value);

        if (readonly) {
            select.addClass("hidden");
        }
        select.removeAttr("id");
        select.addClass("abc");
        var video_item = $("<div class='video-item'></div>");
        video_item.append(select);
        var hidden="<input type='hidden' name='" + video_name + "[]' value='" + video_value + "' id='" + "hidden-hp-" + id + "'>";
        video_item.append(hidden);
        video_item.append("<label class='" + (readonly ? '' : 'hidden') + "'>" + select.find("option:selected").text() + "</label>");
        var html = "";
        html +=             "<input type='text' onchange='updatePreview()' class='form-control "+ (readonly ? 'hidden' : '') + "' name='" + comment_name + "[]' placeholder='comment' value='" + comment_value + "'  />"
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + "</label>";
        video_item.append(html);
        var wrapper = $("<div class='video-item' id='video-item-div-" + id + "' style='margin-bottom: 20px;'></div>");

        var html = "";
        html +=             '<a class="btn photo-item-remove' + (readonly ? ' hidden ' : '') + ' btn-danger" onclick= "video_remove(\'' + id + '\')">';
        html +=                 '<i class="fa fa-trash-o"></i>';
        html +=              '</button>';
        video_item.append(html);
//        wrapper.append(html);
        wrapper.append(video_item);
        $('.photo-comments-list').append(wrapper);
        select.change(function(e) {
            $(e.target).next().val(e.target.value);
            updatePreview();
        });
    }

    function video_remove(id) {
        document.getElementById('video-item-div-' + id).remove();
        updatePreview();
    }

    function add_photo(id, image_name, image_value, preview_value, comment_name, comment_value, is_view = 'readonly') {
        readonly = is_view == 'readonly';

        var html = "<div class='col-md-6' id='photo-item-div-" + id + "' style='margin-bottom: 20px;'>";
        html +=         "<div style='margin-bottom: 10px'>";
        html +=             "<input id='photo-comment-" + id + "' name='" + comment_name + "[]' class='form-control " + (readonly ? ' hidden ' : '') +"' type='text' style='width: 100%; border-radius: 5px !important; ' value='" + comment_value + "' />";
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + "</label>";
        html +=         "</div>";
        html +=         '<div style="margin-bottom: 10px;">';
        html +=             '<img class="photo-item-img" id="photo-image-' + id + '" src="' + preview_value + '" />';
        html +=         '</div>';
        html +=         '<div class="' + (readonly ? ' hidden ' : '') + '">';
        html +=             '<button type="button" class="photo-item-remove btn-danger" onclick= "photo_remove(\'' + id + '\')">';
        html +=                 '<i class="fa fa-trash-o"></i>';
        html +=              '</button>';
        html +=         '</div>';
        html +=         '<input class="hidden" id="photo-image-input-' + id + '" name="' + image_name + '[]" value="'+image_value+'" />';
        html +=     "</div>";

        $('.photo-comments-list').append(html);
    }

    function photo_remove(id) {
        document.getElementById('photo-item-div-' + id).remove();
        updatePreview();
    }
</script>
    <div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home')?>" class="breadcrumb-1"><?= lang('home')?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2">done</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $panel_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-<?=$has_preview ? 9 : 12?>">
                        <p class="content-group">
                            <?= $form_description ?>
                        </p>

                        <form action="<?= $confirm_url ?>" id="layout_form" method="post">
                            <input type="hidden" name="back_url" value="<?= current_url() ?>">
                            <?php if (isset($branch_id)):?>
                            <input type="hidden" name="BRANCH_ID" value="<?= $branch_id ?>">
                            <?php endif;?>

                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <!--Begin News Color Settings-->
                                        <?php foreach($fields as $field) {
                                            echo render_element($field);
                                        }?>
                                    </tbody>
                                </table>
                            </div>
                            <BR>
                            <div class="text-center">
                                <button type="submit" class="btn common-btn-green-small custom-btn">
                                    <?= lang('save') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($has_questions && $id > 0) {
            $this->load->view('_parts/questions', ['SURVEY_ID'=>$id]);
        }
        ?>
        <?php if ($has_preview) {
            $this->load->view('_parts/preview');
        }
        ?>
    </div>
</div>

<script>
    <?php if ($has_preview) {?>
    $(function() {
//        updatePreview();
        $("input, textarea, select").change(function() {
            updatePreview();
        });
    });
    <?php }?>
</script>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>