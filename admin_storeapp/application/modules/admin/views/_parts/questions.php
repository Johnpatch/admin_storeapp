<?php
$rows = $this->db->where("SURVEY_ID", $SURVEY_ID)->order_by("SURVEY_NO")->get("survey_questions")->result_array();
$id_array = [];
foreach ($rows as $row) {
    $id_array[] = $row["ID"];
}
?>

<div class="panel">
    <div class="panel-heading"><?= "List of questions" ?></div>
    <div class="panel-body" style="padding-top:10px;">
        <div class="row">
            <div class="col-md-12">
                <p class="content-group">

                <div class= "text-center">
                    <span class="alert alert-info no-border center">
                        You can set up to 10 queries.
                    </span>
                    <div class="btn-group dropdown-right-bulk ulk-btn-menu">
                    <a href="<?= site_url("admin/survey/publish_question/$SURVEY_ID") ?>"
                    class="btn common-btn-red-small custom-btn"><i
                            class="fa fa-plus"></i>&nbsp;Add</a>
                </div>

               
                <!--table @test start-->
                <?php if (count($rows) > 0): ?>
                    <form method="POST" action="<?= site_url("admin/survey/order_question/$SURVEY_ID") ?>">
                        <input name="orders" type="hidden" id="orders" value='<?=json_encode($id_array)?>'>
                        <div class="table-responsive clear-both">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Correction/Confirmation</th>
                                    <th>Question number</th>
                                    <th>Question</th>
                                    <th>Publishing settings</th>

                                </tr>
                                </thead>
                                <tbody>

                                <tbody id="sortable-questions">
                                <?php foreach ($rows as $row): ?>
                                    <tr data-id="<?= $row["ID"] ?>" class="<?=$row["STATUS_SHOW"] == 'Y' ? 'status-active' : ""?>">
                                        <td align="center"><a
                                                href="<?= site_url("admin/survey/publish_question/$SURVEY_ID/$row[SURVEY_NO]") ?>"
                                                class="btn common-btn-operation-edit custom-btn"><i
                                                    class='fa fa-edit'></i></a></td>
                                        <td align="center"><?= $row["SURVEY_NO"] ?></td>
                                        <td align="center"><?= $row["QUESTION"] ?></td>
                                        <td align="center"><?= radio_input([
                                                "name" => "STATUS_SHOW" . $row["ID"],
                                                "value" => $row["STATUS_SHOW"],
                                                "inline" => true,
                                                "options" => [
                                                    "Y" => "Indicate",
                                                    "N" => "Hidden"
                                                ]
                                            ], false) ?></td>
                                    </tr>

                                <?php endforeach; ?>
                            </table>
                        </div>
                        <div class = "mt-20">
                        </div>
                        <div class="text-center">
                            <span class="alert alert-info no-border center">
                                You can sort by dragging the column.
                            </span>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <button type="submit" class="btn common-btn-green-small custom-btn"
                                    onclick="submitMainForm('')">
                                <?= lang('save') ?></button>
                        </div>
                    </form>
                <?php else: ?>
                
                <div class = "mt-20">
                        </div>
                    <div class="alert alert-warning no-border">
                        <?= lang('no_matching_data_found') ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url("assets/Sortable/js/Sortable.js") ?>"></script>
<script>
    Sortable.create(document.getElementById("sortable-questions"), {
        group: "questions",
        animation: 150,
        store: {
            get: function (sortable) {
//                var order = localStorage.getItem(sortable.options.group);
//                return order ? order.split('|') : [];
                return [];
            },
            set: function (sortable) {
//                console.log(sortable);
                var order = sortable.toArray();
                $("#orders").val(JSON.stringify(order));
//                alert($("#orders").val());
                // $.post(base_url + 'home/change_picture_order', {order:order}, function(data, status){
//                localStorage.setItem(sortable.options.group, order.join('|'));
            }
        },
    });

    $("input[type=radio]").change(function (evt) {
        var val = $(this).parent().find("input[type=radio]:checked").val();
        var tr = $(this).parents("tr");
        if (val == 'Y') {
            tr.addClass("status-active");
        } else {
            tr.removeClass("status-active");
        }
    });

</script>