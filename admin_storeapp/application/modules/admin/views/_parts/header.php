<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>

    <!--    Limitless Styles-->
    <link href="<?=base_url("assets/css/limitless/icons/icomoon/styles.css")?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url("assets/css/limitless/bootstrap.css")?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url("assets/css/limitless/core.css")?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url("assets/css/limitless/components.css")?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url("assets/css/limitless/colors.css")?>" rel="stylesheet" type="text/css">
    <!---->
    <link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?>">

    <link href="<?= base_url('assets/color-picker/css/colpick.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/metronic_library/css/datepicker3.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/metronic_library/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/metronic_library/css/plugins.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/custom-admin.css') ?>" rel="stylesheet">

    <!--    Limitless Scripts-->
    <!-- Core JS files -->
    <script type="text/javascript" src="<?=base_url("assets/js/plugins/loaders/pace.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/js/core/libraries/jquery.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/js/core/libraries/bootstrap.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/js/plugins/loaders/blockui.min.js")?>"></script>
    <!-- /core JS files -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/forms/styling/uniform.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/forms/styling/switchery.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/forms/styling/switch.min.js')?>"></script>

    <script type="text/javascript" src="<?= base_url('assets/js/plugins/ui/moment/moment.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/daterangepicker.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/anytime.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/pickadate/picker.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/pickadate/picker.date.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/pickadate/picker.time.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/bootstrap-datepicker.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/bootstrap-datetimepicker.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/metronic.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/layout.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/quick-sidebar.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/demo.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/components-pickers.js')?>"></script>
    <!-- Theme JS files -->
    <script type="text/javascript" src="<?= base_url("assets/js/core/app.js")?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/pages/form_checkboxes_radios.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/pages/picker_date.js')?>"></script>
    <script src="<?=base_url('assets/js/form-generator.js')?>"></script>
    <!-- /theme JS files -->
    <!---->
</head>
<body>
<?php
    if(!isset($logo)) {
        $logo = base_url("assets/imgs/site-logo.png");
    }
?>
<?php if (isset($logged_in)): ?>
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?= base_url("/").'?token='.$token?>"><img src="<?=$logo?>" alt=""></a>
            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>
        <div class="navbar-collapse collapse" id="navbar-mobile">
            <div class="navbar-right">
                <a href="javascript:void(0);" onclick="logout();" class="logout-btn">
                    <i class="icon-exit"></i><?= lang("logout")?>
                </a>
            </div>
        </div>
    </div>
<?php endif;?>
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <?php if (isset($logged_in)) : ?>
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion left-menu-content">
                            <?php 
                            //if (isset($user->IS_ADMIN) && $user->IS_ADMIN && isset($user->TYPE) && $user->TYPE == 'admin') : ?>
                            <!--
	                            <li class="<?= strstr(uri_string(), 'admin/branch/list') != false ? 'active-page' : '' ?>">
                                    <a href="<?= base_url('admin/branch/list').'?token='.$token?>">
                                        <span><?= lang('branch_list')?></span>
                                    </a>
                                </li>
                                <li class="<?= strstr(uri_string(), 'admin/branch/register') != false ? 'active-page' : '' ?>">
                                    <a href="<?= base_url('admin/branch/register').'?token='.$token?>"> 
                                        <span><?= lang('branch_register_manage')?></span>
                                    </a>
                                </li> -->
                            <?php //endif; ?>
                            <?php if (isset($user->TYPE) && $user->TYPE == 'admin'): ?>
	                            <li class="<?= (uri_string() == 'admin/oem') != false ? 'active-page' : '' ?>">
	                                <a href="<?= base_url('admin/oem').'?token='.$token?>">
	                                    <span><?= lang('oem_list')?></span>
	                                </a>
	                            </li>
	                            <li class="<?= strstr(uri_string(), 'admin/oem/register') != false ? 'active-page' : '' ?>">
	                                <a href="<?= base_url('admin/oem/register').'?token='.$token ?>"> 
	                                    <span><?= lang('oem_register')?></span>
	                                </a>
	                            </li>
                            <?php endif; ?>
                            <?php if (isset($user->TYPE) && $user->TYPE == 'oem'): ?>
                            <li class="<?= strstr(uri_string(), 'admin/branch/list') != false ? 'active-page' : '' ?>">
                                <a href="<?= base_url('admin/branch/list').'?token='.$token ?>">
                                    <span><?= lang('branch_list')?></span>
                                </a>
                            </li>
                            <li class="<?= strstr(uri_string(), 'admin/branch/register') != false ? 'active-page' : '' ?>">
                                <a href="<?= base_url('admin/branch/register').'?token='.$token ?>"> 
                                    <span><?= lang('branch_register_manage')?></span>
                                </a>
                            </li>
                            <li class="<?= strstr(uri_string(), 'admin/setting') != false ? 'active-page' : '' ?>">
                                <a href="<?= base_url('admin/setting').'?token='.$token ?>"> 
                                    <span><?= lang('control_panel')?></span>
                                </a>
                            </li>
                            <?php endif; ?>
                            <!--li class="<?= strstr(uri_string(), 'admin/basic_information') != false ? 'active-page' : '' ?>">
                                <a href="#">
                                    <span><?= lang('admin_basic_information')?></span>
                                </a>
                            </li-->
                            <?php if (isset($user->IS_ADMIN) && $user->IS_ADMIN && isset($user->TYPE) && $user->TYPE == 'admin') : ?>
                            <li class="<?= strstr(uri_string(), 'admin/setting') != false ? 'active-page' : '' ?>">
                                <a href="<?= base_url('admin/setting').'?token='.$token ?>"> 
                                    <span><?= lang('control_panel')?></span>
                                </a>
                            </li>
                            <?php endif; ?>
                            <li class="<?= strstr(uri_string(), 'admin/profile') != false ? 'active-page' : '' ?>">
                                <a href="<?= base_url('admin/profile').'?token='.$token ?>"> 
                                    <span><?= lang('profile')?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
<?php endif;?>


<script type="text/javascript">
            <?php if (!$logged_in) {
                ?>
                logout();
                <?php
            } 
            ?>
            function logout() {
                window.parent.postMessage('data' , 'https://docodoor-app.firebaseapp.com');
            }
</script>