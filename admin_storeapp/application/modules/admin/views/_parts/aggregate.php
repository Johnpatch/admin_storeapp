
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home')?>" class="breadcrumb-1"><?= lang('home')?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> done</li>
                </ul>
            </div>
        </div>
    </div>
    <?php if ($has_search) :?>
        <div class="">
            <div class="panel">
                <div class="panel-heading">
                    <?= lang('search_condition') ?>
                </div>
                <div class="panel-body" style="padding-top:10px;">
                    <div class="row">
                        <div class="col-md-9">
                            <p class="content-group">
                                <?= $form_description ?>
                            </p>
                            <form method="GET" id="searchProductsForm" action="" class="form-horizontal form-bordered">
                                <div class="search-condition-content">
                                    <table class="table table-bordered" style="background-color: #FFF;">
                                        <tbody>
                                        <?php foreach($search_fields as $field) {
                                            echo render_element($field);
                                        }?>
                                        <tr>
                                            <td colspan="2" valign="center">
                                                <div class="col-md-12 text-center">
                                                    Number of search results displayed
                                                    <?=perpage_select('perpage', $perpage_options, $perpage)?>
                                                    <button type="submit" class="btn common-btn-search custom-btn"><i class="fa fa-search"></i>&nbsp;&nbsp;<?= lang('search') ?></button>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $page_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-9">
                        <p class="content-group">
                            <?php if($title_index):?>
                                <?=$list_description?>
                            <?php else:?>
                                <?=$list_description?>
                            <?php endif;?>
                        </p>
                        <?php if($has_order || $has_add) :?>
                            <div class = "mb-20">
                                <?php if ($has_add):?>
                                    <a href='<?= $add_url ?>' class='common-btn-green-small custom-btn'>
                                        <i class="fa fa-plus-circle"></i>&nbsp;&nbsp;<?= lang('sign_up') ?>
                                    </a>
                                <?php endif;?>
                                <?php if ($has_order):?>
                                    <div class="btn-group dropdown-right-bulk ulk-btn-menu">
                                        <a type="button" class="btn common-btn-red-small custom-btn" data-toggle="dropdown"><i class="fa fa-check-circle"></i>&nbsp;bulk update</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a id="bulk-action" onclick="$('#order-update').submit()"><i class="fa fa-refresh"></i> Bulk Update</a></li>
                                        </ul>
                                    </div>
                                <?php endif;?>
                            </div>
                        <?php endif;?>
                        <!--table @test start-->
                        <div class="table-responsive clear-both">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <?php foreach ($table_fields as $column):?>
                                        <th align="<?=element('align', $column, "center")?>"><?= $column['label'] ?></th>
                                    <?php endforeach;?>
                                </tr>
                                </thead>
                                <tbody>
                                <form id="order-update" method="POST" action="<?=$order_url?>">
                                    <?php foreach ($rows as $row):?>
                                        <tr>
                                            <?php foreach ($table_fields as $column):?>
                                                <td align="<?=element('align', $column, "center")?>" class="td-valign">
                                                    <?php $type = element('type', $column);
                                                    $group = element('group', $column);
                                                    $between = element('between', $column);
                                                    ?>
                                                    <?php if ($group == false):?>
                                                        <?php if ($type == "edit"):?>
                                                            <a href="<?=$add_url.$row["ID"]?>" class="btn common-btn-operation-edit custom-btn"><i class='fa fa-edit'></i></a>
                                                        <?php elseif ($type == "delete"):?>
                                                            <a href="<?=$delete_url.$row["ID"]?>" class="btn common-btn-operation-trash custom-btn"><i class='fa fa-trash'></i></a>
                                                        <?php elseif ($type == "order"):?>
                                                            <input type="number" class="form-control order-input" name="ORDER[]" value="<?=$row["ORDER"]?>">
                                                            <input type="hidden" name="ID[]" value="<?=$row["ID"]?>">
                                                        <?php elseif ($type == "image"):?>
                                                            <img src="<?=base_url($row[$column["name"]])?>" alt="No Image" class="img-thumbnail" style="height:100px;">
                                                        <?php else:?>
                                                            <?=content_view($column, $row[$column["name"]])?>
                                                        <?php endif;?>
                                                    <?php else:?>
                                                        <?php
                                                        $is_first = true;
                                                        foreach ($column["input"] as $field) {
                                                            if (!$is_first) echo $column["between"];
                                                            echo content_view($field, $row[$field["name"]]);
                                                            $is_first = false;
                                                        }
                                                        ?>
                                                    <?php endif;?>
                                                </td>

                                            <?php endforeach;?>
                                        </tr>
                                    <?php endforeach;?>
                                </form>
                                </tbody>
                            </table>
                        </div>
                        <?= $links_pagination ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


