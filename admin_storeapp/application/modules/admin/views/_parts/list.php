<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home') ?>" class="breadcrumb-1"><?= lang('home') ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done') ?></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $page_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-<?= $has_preview ? 9 : 12 ?>">
                        <p class="content-group">
                            <?php if ($title_index): ?>
                                <?= $list_description ?>
                            <?php else: ?>
                                <?= $list_description ?>
                            <?php endif; ?>
                        </p>
                        <?php if ($has_order || $has_add) : ?>
                            <div class="mb-20">
                                <?php if ($has_add): ?>
                                    <a href='<?= $add_url ?>' class='common-btn-green-small custom-btn'>
                                        <i class="fa fa-plus-circle"></i>&nbsp;&nbsp;<?= lang('sign_up') ?>
                                    </a>
                                <?php endif; ?>
                                <?php if ($has_order): ?>
                                    <div class="btn-group dropdown-right-bulk ulk-btn-menu">
                                        <a type="button" class="btn common-btn-red-small custom-btn"
                                           data-toggle="dropdown"><i class="fa fa-check-circle"></i>&nbsp;bulk
                                            update</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a id="bulk-action" onclick="$('#order-update').submit()"><i
                                                        class="fa fa-refresh"></i> Bulk Update</a></li>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <!--table @test start-->
                        <?php if (count($rows) > 0): ?>
                            <div class="table-responsive clear-both">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <?php foreach ($table_fields as $column): ?>
                                            <th align="<?= element('align', $column, "center") ?>">
                                                <?= $column['label'] ?>
                                                <?php
                                                $sortable = element("sortable", $column);
                                                if ($sortable == true):?>
                                                    <?php $direction = $this->input->get($column["name"]);
                                                    if ($direction != "ASC"):?>
                                                        <a onclick="$('input[name=<?= $column["name"] ?>]').val('ASC'); $('#searchForm').submit();"><i
                                                                class="icon-arrow-down7"></i></a>
                                                    <?php endif; ?>
                                                    <?php if ($direction != "DESC"): ?>
                                                        <a onclick="$('input[name=<?= $column["name"] ?>]').val('DESC'); $('#searchForm').submit();"><i
                                                                class="icon-arrow-up7"></i></a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </th>
                                        <?php endforeach; ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <form id="order-update" method="POST" action="<?= $order_url ?>">
                                        <?php foreach ($rows as $row): ?>
                                            <?php
                                            $class = "";
                                            foreach ($table_fields as $column) {

                                                $class_options = element("class", $column);
                                                if ($class_options != false) {
                                                    foreach ($class_options as $key => $value) {
                                                        if ($key == $row[$column["name"]]) $class .= " $value";
                                                    }
                                                }
                                            }; ?>
                                            <tr class="<?= $class ?>">
                                                <?php foreach ($table_fields as $column): ?>
                                                    <td align="<?= element('align', $column, "center") ?>"
                                                        class="td-valign">
                                                        <?php $type = element('type', $column);
                                                        $group = element('group', $column);
                                                        $between = element('between', $column);
                                                        $has_fixed = element("has_fixed", $column);
                                                        ?>
                                                        <?php if ($group == false): ?>
                                                            <?php if ($type == "edit"): ?>
                                                                <a href="<?= $add_url . $row["ID"] ?>"
                                                                   class="btn common-btn-operation-edit custom-btn"><i
                                                                        class='fa fa-edit'></i></a>
                                                            <?php elseif ($type == "delete"): ?>
                                                                <?php if (!$has_fixed || $row["TYPE"] > 0): ?>
                                                                    <a href="<?= $delete_url . $row["ID"] ?>"
                                                                       class="btn common-btn-operation-trash custom-btn confirm-delete"><i
                                                                            class='fa fa-trash'></i></a>
                                                                <?php endif; ?>
                                                            <?php elseif ($type == "order"): ?>
                                                                <input type="number" class="form-control order-input"
                                                                       name="ORDER[]" value="<?= $row["ORDER"] ?>">
                                                                <input type="hidden" name="ID[]"
                                                                       value="<?= $row["ID"] ?>">
                                                            <?php elseif ($type == "image"): ?>
                                                                <img src="<?= base_url($row[$column["name"]]) ?>"
                                                                     alt="No Image" class="img-thumbnail"
                                                                     style="height:100px;">
                                                            <?php else: ?>
                                                                <?= content_view($column, $row[$column["name"]]) ?>
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <?php
                                                            $is_first = true;
                                                            foreach ($column["input"] as $field) {
                                                                if (!$is_first) echo $column["between"];
                                                                echo content_view($field, $row[$field["name"]]);
                                                                $is_first = false;
                                                            }
                                                            ?>
                                                        <?php endif; ?>
                                                    </td>

                                                <?php endforeach; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    </form>
                                    </tbody>
                                </table>
                            </div>
                            <?= $links_pagination ?>
                        <?php else: ?>
                            <div class="alert alert-warning no-border">
                                <?= lang('no_matching_data_found') ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($has_preview) $this->load->view('_parts/preview') ?>
    </div>
    <?php if ($has_search) : ?>
        <div class="">
            <div class="panel">
                <div class="panel-heading">
                    <?= lang('search_condition') ?>
                </div>
                <div class="panel-body" style="padding-top:10px;">
                    <div class="row">
                        <div class="col-md-<?= $has_preview ? 9 : 12 ?>">
                            <p class="content-group">
                                <?= $form_description ?>
                            </p>

                            <form method="GET" id="searchForm" action="" class="form-horizontal form-bordered">
                                <div class="search-condition-content">
                                    <table class="table table-bordered" style="background-color: #FFF;">
                                        <tbody>
                                        <?php foreach ($search_fields as $field) {
                                            echo render_element($field);
                                        } ?>
                                        <tr>
                                            <td colspan="2" valign="center">
                                                <div class="col-md-12 text-center">
                                                    Number of search results displayed
                                                    <?= perpage_select('perpage', $perpage) ?>
                                                    <button type="submit" class="btn common-btn-search custom-btn"><i
                                                            class="fa fa-search"></i>&nbsp;&nbsp;<?= lang('search') ?>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <?php if (is_array($table_fields)) {
                                    foreach ($table_fields as $field) {
                                        if (element("sortable", $field) == true) {
                                            ?>
                                            <input type="hidden" name="<?= $field["name"] ?>"
                                                   value="<?= $this->input->get($field["name"]) ?>">
                                        <?php }
                                    }
                                } ?>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($has_preview) : ?>
        <form id="layout_form" style="display:none;" method="POST" action="<?= $preview_url ?>" target="preview-frame">
            <input type="hidden" name="BRANCH_ID" value="<?= $branch_id ?>">
            <input type="hidden" name="back_url" value="<?=current_url()?>">
        </form>
    <?php endif; ?>
    <script>

        $(function () {
            <?php if($has_preview) :?>

            $('#layout_form').submit();
            <?php endif; ?>
            $("#bulk-dropdown").click(function () {
                $("#bulk-action").toggleClass("hide");
            })


        });
    </script>
