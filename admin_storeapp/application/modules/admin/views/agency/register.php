<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('agency_register_manage')?></span>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/profile')?>" class="breadcrumb-1"><?= lang('home')?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><?= lang('agency_register_manage')?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-9">
                        <p class="content-group">
                            ※発注はユーザ管理画面でストアー申請をした日、または非公開アプリ用のURLを発行した日が自動的に発注日になります。月額課金開始日はその日の翌月1日からになります。 ※発注されたら本社からメールで請求書をお送りします。支払い日は発注日の月末までに本社の指定銀行口座にお支払いください。
                        </p>
                        <form action="<?=base_url('admin/agency/submit')?>" id="layout_form" method="post">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <tr>
                                            <td><?= lang("agency_id")?></td>
                                            <td class="login_id_td">
                                                <input type="text" name="LOGIN_ID" class="form-control" value="<?=$agency && isset($agency->LOGIN_ID) ? $agency->LOGIN_ID : ''?>">
                                                <span class="required_span help-block hide" id="id_message"><?= lang("required_field")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("password")?></td>
                                            <td class="password_td">
                                                <input type="password" name="PASSWORD" class="form-control" value="">
                                                <span class="required_span help-block hide"><?= lang("required_field")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("confirm_password")?></td>
                                            <td class="confirm_password_td">
                                                <input type="password" name="CONFIRM_PASSWORD" class="form-control">
                                                <span class="required_span help-block hide"><?= lang("confirm_password")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("domain")?></td>
                                            <td class="domain_td">
                                                <input type="text" name="DOMAIN" class="form-control" value="<?=$agency && isset($agency->DOMAIN) ? $agency->DOMAIN : ''?>">
                                                <span class="required_span help-block hide"><?= lang("required_field")?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("total_oems")?></td>
                                            <td>
                                                <input type="text" name="TOTAL_OEMS" class="form-control" value="<?=$agency && isset($agency->TOTAL_OEMS) ? $agency->TOTAL_OEMS : '0'?>" readonly>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?= lang("total_branches")?></td>
                                            <td>
                                                <input class='form-control' name='TOTAL_BRANCHES' size='16' type='text' value="<?=$agency && isset($agency->TOTAL_BRANCHES) ? $agency->TOTAL_BRANCHES : '0'?>" readonly/>
                                            </td>
                                        </tr>
                                        <div class="hidden">
                                            <input name="ADMIN_YN" value="N">
                                            <input name="ID" value="<?=$agency && isset($agency->ID) ? $agency->ID : ''?>">
                                        </div>
                                    </tbody>
                                </table>
                            </div>
                            <BR>
                            <div class="text-center">
                                <button type="button" onclick="submitAgency();" class="btn common-btn-green-small custom-btn">
                                    <?= lang('save') ?></button>
                                <a href="javascript:history.back(-1);" class="btn common-btn-gray-small custom-btn">
                                    <?= lang('cancel') ?></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var default_id = "<?=$agency && isset($agency->LOGIN_ID) ? $agency->LOGIN_ID : ''?>";
    function submitAgency() {
        var is_validate = true;
        var id_message_empty = "<?=lang('required_field')?>";
        var id_message_exist = "<?=lang('exist_id')?>";
        $('#id_message').html('');

        if($("input[name='LOGIN_ID']").val() == "") {
            $("td.login_id_td").addClass("has-error");
            $("td.login_id_td .required_span").removeClass("hide");
            $('#id_message').html(id_message_empty);
            is_validate = false;
        } else {
            $("td.login_id_td").removeClass("has-error");
            $("td.login_id_td .required_span").addClass("hide");
        }

        if($("input[name='DOMAIN']").val() == "") {
            $("td.domain_td").addClass("has-error");
            $("td.domain_td .required_span").removeClass("hide");
            is_validate = false;
        } else {
            $("td.domain_td").removeClass("has-error");
            $("td.domain_td .required_span").addClass("hide");
        }

        if($("input[name='PASSWORD']").val() == "" && $("input[name='ID']").val() == "") {
            $("td.password_td").addClass("has-error");
            $(".password_td .required_span").removeClass("hide");
            is_validate = false;
        } else {
            $("td.password_td").removeClass("has-error");
            $(".password_td .required_span").addClass("hide");
            if ($("input[name='PASSWORD']").val() != $("input[name='CONFIRM_PASSWORD']").val()) {
                $("td.confirm_password_td").addClass("has-error");
                $(".confirm_password_td .required_span").removeClass("hide");
                is_validate = false;
            } else {
                $("td.confirm_password_td").removeClass("has-error");
                $(".confirm_password_td .required_span").addClass("hide");
            }
        }

        if (is_validate == false) return false;

        var input_id = $('input[name="LOGIN_ID"]').val();
        if (default_id == input_id) {
            $("#layout_form").submit();
            return true;
        }
        else {
            $.ajax({
                type: "POST",
                url: "<?=base_url('admin/profile/checkExist')?>",
                data: {id: input_id}
            }).done(function (data) {
                if (JSON.parse(data).exist == 'true') {
                    $("td.login_id_td").addClass("has-error");
                    $("td.login_id_td .required_span").removeClass("hide");
                    $('#id_message').html(id_message_exist);
                    return false;
                }

                $("td.login_id_td").removeClass("has-error");
                $("td.login_id_td .required_span").addClass("hide");
                $("#layout_form").submit();

            });
        }
    }
</script>