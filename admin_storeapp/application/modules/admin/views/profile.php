<style>
    #message {
        margin-top: 10px;
        padding: 5px;
    }
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('profile')?></span></h4>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('/').'?token='.$token ?>" class="breadcrumb-1"><?= lang('home') ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"></a><?= lang("profile")?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel">
        <?php
          //  $user = $this->session->userdata('user');
        ?>
        <div class="panel-body" style="display: block;">
            <form class="form-horizontal" action="profile/submit?token=<?= $token ?>" method="post" id="submit_form">
                <div class="form-group">
                    <label class="control-label col-lg-2"><?=lang('login_id')?></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?=$user->USERID?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2"><?=lang('password')?></label>
                    <div class="col-lg-10">
                        <input type="password" class="form-control" name="PASSWORD" id="password">
                        <div class="alert alert-danger hidden" id="message"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2"><?=lang('confirm_password')?></label>
                    <div class="col-lg-10">
                        <input type="password" class="form-control" value="" id="confirm">
                    </div>
                </div>
                <div class="hidden">
                    <input type="text" name="ID" value="<?=$user->ID?>">
                    <input type="text" name="TYPE" value="<?=$user->TYPE?>">
                </div>
                <div class="text-right">
                    <button class="btn btn-primary"><?=lang("update")?> <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#submit_form').submit(function() {
            var password = $('#password').val();
            var confirm = $('#confirm').val();

            if (!password) {
                warning('<?=lang('message_empty')?>');
                return false;
            } else if (password != confirm) {
                warning('<?=lang('message_match')?>');
                return false;
            } else if (password.length < 6) {
                warning('<?=lang('message_length')?>');
                return false;
            }
        });
    });
    function warning(message) {
        console.log(message);
        $('#message').html(message);
        $('#message').removeClass('hidden');
    }
</script>