<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends AbstractModel
{
    var $_table = "basic_setting";
    var $_pk = "ID";
    var $_sort = "ID";
}
