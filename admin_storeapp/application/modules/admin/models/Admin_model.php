<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends AbstractModel
{
    var $_table = "admin";
    var $_pk = "ID";
    var $_sort = "ID";

    function all($filter = array(), $order = array(), $direction = 'asc', $fields = "*") {
		if(!empty($filter['search'])) {
            $this->db->like('LOGIN_ID', $filter['search'], 'both');
            $this->db->or_like('DOMAIN', $filter['search'], 'both');
        }
		return parent::all($filter, $order);
	}

	function count($filter = array(), $order = array(), $direction = 'asc', $fields = "*") {
		if(!empty($filter['search'])) {
            $this->db->like('LOGIN_ID', $filter['search'], 'both');
            $this->db->or_like('DOMAIN', $filter['search'], 'both');
        }
		return parent::count($filter, $order);
	}

}
